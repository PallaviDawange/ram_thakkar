 <div class="header collapse d-lg-flex p-0" id="headerMenuCollapse">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-1 ml-auto">
                <form class="input-icon my-3 my-lg-0">
                  
                 <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><img style="width:50px;height:50px"; src="<?php echo base_url(); ?>assets/images/prof.svg"  style="float-right";/></a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <!-- <a href="<?php echo base_url();?>change_password" class="dropdown-item "> Change Password</a> -->
                      <a href="<?php echo base_url();?>logout" class="dropdown-item ">Logout</a>
                    </div>
                  </li>
                </form>
              </div>
              <div class="col-lg order-lg-first">
                <ul class="nav nav-tabs border-0 flex-column flex-lg-row">
                     <!-- <li class="nav-item">
                    <a href="<?php echo base_url();?>dashboard" class="nav-link"><i class="fe fe-home"></i> Dashboard</a>
                  </li> -->

                 <li class="nav-item">
                    <a href="<?php echo base_url();?>show_registration" class="nav-link"><i class="fe fe-book"></i>Inward and Outward Register</a>
                  </li>
                  <li class="nav-item">
                    <a href="<?php echo base_url();?>show_signature" class="nav-link"><i class="fe fe-minus-square"></i> Digital signature</a>
                  </li>


                  <li class="nav-item">
                    <a href="<?php echo base_url();?>show_company" class="nav-link"><i class="fa fa-bank"></i> Company Master</a>
                  </li>

                   <li class="nav-item">
                    <a href="<?php echo base_url();?>show_staff" class="nav-link"><i class="fe fe-user"></i> Staff Master</a>
                  </li>
                   <li class="nav-item">
                    <a href="<?php echo base_url();?>serviceMaster" class="nav-link"><i class="fe fe-command"></i> Service Master</a>
                  </li>
                   <li class="nav-item">
                    <a href="<?php echo base_url();?>show_time" class="nav-link"><i class="fa fa-calendar"></i>Time Sheet</a>
                  </li>
<!-- 
                   <li class="nav-item dropdown"> 
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-user"></i>Client Master</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="<?php echo base_url();?>add_client" class="dropdown-item ">Add Client</a>
                      <a href="<?php echo base_url();?>show_client" class="dropdown-item ">Show Client</a>
                    </div>
                  </li>         -->          
                                  
                  <li class="nav-item dropdown">
                    <a href="javascript:void(0)" class="nav-link" data-toggle="dropdown"><i class="fe fe-file"></i>Reports</a>
                    <div class="dropdown-menu dropdown-menu-arrow">
                      <a href="<?php echo base_url();?>staff_wise_report" class="dropdown-item ">Staff Wise Report</a>
                      <a href="<?php echo base_url();?>company_wise_report" class="dropdown-item ">Company wise Report </a>                      
                    </div>

                    </li>         
                    
                   
                </ul>
              </div>
            </div>
          </div>
        </div>