 <footer class="footer">
        <div class="container">
          <div class="row align-items-center flex-row-reverse">
            <div class="col-auto ml-lg-auto">
              <div class="row align-items-center">
                <div class="col-auto">
                  <!--<ul class="list-inline list-inline-dots mb-0">-->
                  <!--  <li class="list-inline-item"><a href="<?php echo base_url();?>docs/index.html">Documentation</a></li>-->
                  <!--  <li class="list-inline-item"><a href="<?php echo base_url();?>faq.html">FAQ</a></li>-->
                  <!--</ul>-->
                </div>
                <!--<div class="col-auto">-->
                <!--  <a href="https://github.com/tabler/tabler" class="btn btn-outline-primary btn-sm">Source code</a>-->
                <!--</div>-->
              </div>
            </div>
            <div class="col-12 col-lg-auto mt-3 mt-lg-0 text-center">
               All rights reserved © 2018. <a href="http://www.alphaobs.com" target="blank" >ALPHA OBS</a>.
            </div>
          </div>
        </div>
      </footer>