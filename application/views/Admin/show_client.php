<!doctype html>
<html lang="en" dir="ltr">
  <head>
<?php include('include/header_datatable.php'); ?>
  </head>
  <?php include('include/nav.php'); ?>
  <body>
   <div class="container" style="margin:20px auto">
     <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
       
              <?php
            echo form_open('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                     
                <div class="card">
                  <table class="table card-table table-vcenter" id="myTable">
                      <thead>
                    <tr>
                     
                     <th><b>Sr. No.</b></th>
                      <th><b>Client Name</b></th>
                      <th><b>Company Name</b></th>
                      <th><b>Mobile Number</b></th>                       
                       <th><b>View Details</b></th>
                     
                    </tr>
                    </thead>
                    <?php //  print_r($client_data); ?>
                    <tbody id="table1">
                        <?php if(count($client_data) > 0)
                                         {  
                                             $i=1;
                                           foreach($client_data as $row)
                                       { 
                                         $encrypted_client_id = $this->encrypt->encode($row->client_id);
                                         $encrypted_client_id = strtr($encrypted_client_id,array('+' => '.', '=' => '-', '/' => '~'));
                                         
                                        ?>                    
                    <tr>
                      <td><?php echo $i++;?></td>
                       <td><?php echo $row->client_name;?></td>
                      <td><?php echo $row->company;?></td>
                      <td><?php echo $row->mobile;?></td>                      
                       <td>
                        <a href="<?php echo base_url();?>client_service_dashboard/<?php echo $encrypted_client_id;?>" style="color:white;font-size:10px;"   class="btn btn-success" >Dashboard</a>

                        <a href="<?php echo base_url();?>assign_client_service/<?php echo $encrypted_client_id;?>" style="color:black;font-size:20px;"  title="Add Services" ><i class="fa fa-plus"></i></a>

                        <a href="<?php echo base_url();?>edit_client_details/<?php echo $encrypted_client_id;?>" style="color:#00BFFF;font-size:20px;"  title="View more Details" ><i class="fa fa-pencil"></i></a>   
                       
                        <a href="#"  data-href="<?php echo base_url();?>delete_client_details/<?php echo $encrypted_client_id;?>" style="color:red;font-size:20px;"  title="Delete Client"  data-toggle="modal" data-target="#confirm-delete"><i class="fa fa-trash"></i></a>
                        </td>   
                    </tr>
                    
                      <?php   }} ?>
                     </tbody>
                  </table>

                  <!-- MODAL -->
            
<div class="modal fade show" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content panel-info panel-color">
      
          <div class="modal-header panel-info panel-color">             
              <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
               <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
          </div>
      
          <div class="modal-body">
              <p>You are about to delete one record, this procedure is irreversible.</p>
              <p>Do you want to proceed?</p>
                 </div>
          
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <a class="btn btn-danger btn-ok">Delete</a>
          </div>
      </div>
  </div>
</div>
<?php include "Ajax/customdelete.php"; ?>
                  <script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>
     <script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>        
                </body>
                </html>