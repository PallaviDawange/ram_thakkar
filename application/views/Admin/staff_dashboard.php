<head><?php include('include/headers.php'); ?>
<style>
</style>

  </head>
  
  <?php include('include/nav-client.php'); ?>
<body>
  <div class="my-3 my-md-5">
    <div class="container">


          <div align="center">
          
              <div class="card-body">
                 <div class="row" align="center">
                  <div class="col-sm-12">
                    <div class="card">
                      <div class="card-header">
                        <h3 class="card-title"><b>Task Count</b></h3>
                      </div>
                      <div class="card-body">
                          <h4> Number of Task Assigned :&nbsp;<span style="color:orange;">50</span></h4>
                           <h4> Number of Task Completed :&nbsp;<span style="rgb(94, 186, 0);">50</span></h4>
                          
                        <div id="chart-donut" class="mr-auto" style="height:13rem";>
                         
                        </div>
                      </div>
                    </div>
                    <script>
                      require(['c3', 'jquery'], function(c3, $) {
                        $(document).ready(function(){
                          var chart = c3.generate({
                            bindto: '#chart-donut', 
                            data: {
                              columns: [
                                  // each columns data
                                ['data1',50 ],
                                ['data2', 50]
                              ],
                              type: 'donut', // default type of chart
                              colors: {
                                'data1': tabler.colors["green"],
                                'data2': tabler.colors["orange"]
                              },
                              names: {
                                  // name of each serie
                                'data1': 'Number of Task Completed',
                                'data2': 'Number of Task Assigned'
                              }
                            }
                            ,
                            axis: {
                            },
                            legend: {
                                      show: false, //hide legend
                            },
                            padding: {
                              bottom: 0,
                              top: 0
                            },
                          });
                        });
                      });
                    </script>
                  </div>
              </div>
            </div>
          </div>
        


                  <div class="card">
                      <div class="card-header text-center">
                      <h1 class="card-title mx-auto text-success font-weight-bold display1">Tasks</h1>
                      </div>
                    <div class="card-body">
                      <table class="table card-table table-vcenter" id="myTable">
                    <tr>
                      <th>Sr. No.</th>
                      <th>Service Name</th>
                      <th>Company Name</th>
                      <th>Task Date</th>
                      <th>Stage</th>
                      <th>Checklist</th>
                      <th>Remark</th>
                    </tr>
                      <?php //if(count($ghosts) > 0 ) { 
                      //   $i=1;
                      //      foreach( $ghosts as $ghost ){?>

                      <tr>
                      <td><?php echo "1";?></td>
                      <td><?php echo "Income Tax";?></td> 
                      <td><?php echo "Alpha Obs";?></td>           
                      <td><?php echo "10/06/2019";?></td>
                      <td><select type="text" class="form-control" required="" value="" id="type">
                      <option value="">Select</option>
                      <option value="inward">Stage 1</option>
                      <option value="outward">Stage 2</option>
                      <option value="outward">Stage 3</option>
                      <option value="outward">Stage 4</option>
                      </select></td>      
                      <td><?php echo "Testing" ?></td>
                      <td>
                      <a href="#" style="color:white;font-size:10px;"   class="btn btn-primary" >Mark as Completed</a>
                      </td>
                      <?php //} }?>
                      </tr>
                      </table>
              </div>     
            </div>
  </div> 
  </div> 
</body>

<div class="modal fade show" id="confirm-delete" role="dialog">
    <div class="modal-dialog modal-md">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Checklist</h4>
          <button type="button" class="close" data-dismiss="modal"></button>
        </div>
        <div class="modal-body">
          <form>
             <div class="checkbox">
      <label><input type="checkbox" checked="checked" value="">Aadhar Card</label>
    </div>
    <div class="checkbox">
     <label><input type="checkbox" value="">Pan Card</label>
    </div>
    <div class="checkbox disabled">
      <label><input type="checkbox" value="" disabled>Income Certificate</label>
    </div>
  </form>
        </div>
        <span id="error"></span>
        <div class="modal-footer col-md-12">
            <button type="button" id="yes" onclick="make();" class="btn btn-success" >Submit</button>
              <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>
 <script>
    function change(id){
    var status = $('#status').val();
    //alert(id);
    
    $.ajax({
    cache: false,
    type: 'POST',
    url: '<?php echo base_url()?>Client_management/update_service_status',
    data: 'id='+id+'&status='+status,
    success: function(data) 
    {
      //alert(data);
    }
    
    });
    
    
    }



</script>

<script type="text/javascript">
  function make()
  {
    $('#error').html("<p style='color:red;'>You have sent Email Successfully.</p>");
  }
</script>
<script>
ocument).ready(function(){
    $('#myTable').dataTable();
});

$('#myTable').dataTable( {
  "ordering": false
} );
</script>    