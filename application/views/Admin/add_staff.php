<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
     <style>
#star{
    color: red;
    font-size:20px;
}
label.error{
    color: red;
    font-size: 12px;
}
  </style>
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>

              <?php
            echo form_open_multipart('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6>
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Add Staff<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Name of Staff<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="staff_name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Mobile No.<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="mobile">
                      </div>
                    </div>
                
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Designation<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="designation">
                      </div>
                    </div>
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Address<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="address">
                      </div>
                    </div>
                   
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Username<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="username">
                      </div>
                    </div>
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Password<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="password">
                      </div>
                    </div>
                        
                   
                     
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>   
             


<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>