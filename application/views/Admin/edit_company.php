<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
   
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
              <?php
            echo form_open_multipart('update_client',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Edit Company<b></h3>
                  <div class="row" align="center">
                    <?php 
                    foreach ($company_data as $key) {
                    $email= json_decode($key->email);
                    $dir= json_decode($key->director_name);
                      //print_r($email);
                      # code...
                    
                     ?>
                 
                        <input type="hidden" class="form-control"  value="<?php echo $company_data[0]->comp_id ?>" name="id">
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Company Name</label>
                        <input type="text" class="form-control"  value="<?php echo $company_data[0]->company_name ?>" name="client_name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Identification No</label>
                        <input type="text"  class="form-control"  value="<?php echo $company_data[0]->id_no ?>" name="company_name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Director Name</label>
                        <input type="text" class="form-control"  value="<?php echo $company_data[0]->director_name ?>" name="gst">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Email</label>
                        <input type="text" class="form-control" value="<?php echo $company_data[0]->email ?>" name="location" >
                      </div>
                    </div>
                   
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Landline No.</label>
                        <input type="text" class="form-control" value="<?php echo $company_data[0]->landline ?>" name="mobile">
                      </div>
                    </div>
                   
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Address</label>
                        <input type="text" class="form-control"  value="<?php echo $company_data[0]->address ?>" name="address">
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Mobile No.</label>
                        <input type="text" class="form-control" value="<?php echo $company_data[0]->mobile ?>" name="mobile">
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Remark</label>
                        <input type="text" class="form-control" value="<?php echo $company_data[0]->remark ?>" name="mobile">
                      </div>
                    </div>  
                    <?php } ?>                                
                               
            <div class="card-footer text-center">
              <button type="submit" name="save" class="btn btn-success">Update</button>                  
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>                
                

<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>