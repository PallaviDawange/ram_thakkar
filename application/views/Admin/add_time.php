<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
     <style>
#star{
    color: red;
    font-size:20px;
}
label.error{
    color: red;
    font-size: 12px;
}
  </style>
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>

              <?php
            echo form_open_multipart('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6>
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Add Time<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Task Name<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="company_name">
                        <button type="button" id="check-minutes">Check the minutes</button>

                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Start Time.<span id="star">*</span></label>
                        <input class="form-control" id="single-input" value="" placeholder="Now">
                      </div>
                    </div>
                
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">End Time</label>
                        <input type="text" class="form-control" required="" value="" name="landline">
                      </div>
                    </div>
                     
                     
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>   
             <script>
    $(document).ready(function(){  
        $("#addmore").click(function(){  
            $("#field-increase").append(
                '<div class="row">'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<input type="text" class="form-control" name="bill_number[]" required>'
                +'</div>'
                +'</div>'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<input class="form-control datepicker" type="text" name="bill_date[]" id="payment_date" required>'                                                      
                +'</div>'
                +'</div>'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<button class="btn btn-danger" id="remove">Remove</button>'
                +'</div>'
                +'</div>'
                +'</div>'
            );  
             $(".datepicker").datepicker({
            format: 'dd/mm/yyyy',
             });
        });
        $(document).on("click","#remove",function(){
            $(this).closest('.row').remove();
        });
    });
    </script>
    <script type="text/javascript">
var input = $('#single-input').clockpicker({
    placement: 'bottom',
    align: 'left',
    autoclose: true,
    'default': 'now'
});

// Manually toggle to the minutes view
$('#check-minutes').click(function(e){
    // Have to stop propagation here
    e.stopPropagation();
    input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
});
</script>



<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>