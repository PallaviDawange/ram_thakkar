<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
   
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
              <?php
            echo form_open_multipart('save_service',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Add Service<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Service Name</label>
                        <input type="text" class="form-control" required="" value="" name="service_name">
                      </div>
                    </div>
                 
                    </div>
                    <div class="col-sm-12 col-md-12 button dropdown">
                      <div class="form-group">
                        <label class="form-label">Frequency</label>
                        <select name="frequency" class="form-control" required="" id="ddlPassport">
                          <option value="">Select</option>
                          <option value="Monthly">Monthly</option>
                          <option value="Quarterly">Quarterly</option>
                          <option value="Annually">Annually</option>
                           <option value="Monthly">Non Routine Service</option>
                        </select>
                      </div>
                    </div>

                    <div class="col-sm-12 col-md-12" id="month" style="display: none">
                      <div class="form-group">
                        <label class="form-label">Select Date</label>
                         <select name="monthly_date" class="form-control 1-31"  id="">  
                         <option value=" ">Select</option>                     
                       </select>
                      </div>
                    </div>  
                    <div class="row col-md-12" id="quterly" style="display: none">
                    <div class="col-sm-6 col-md-6"  >
                      <div class="form-group">
                        <label class="form-label">Select Date 1</label>
                         <select name="quterly_date1" class="form-control 1-31"  id=""> 
                          <option value=" ">Select</option>                       
                       </select>
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6" >
                      <div class="form-group">
                        <label class="form-label">Select Month 1</label>
                         <select name="quterly_month1" class="form-control" id="">
                           <option value=" ">Select</option> 
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>                     
                       </select>
                      </div>
                    </div> 
                    <div class="col-sm-6 col-md-6"  >
                      <div class="form-group">
                        <label class="form-label">Select Date 2</label>                         
                         <select name="quterly_date2" class="form-control 1-31"  id="">    
                          <option value=" ">Select</option>                    
                       </select>
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6" >
                      <div class="form-group">
                        <label class="form-label">Select Month 2</label>
                         <select name="quterly_month2" class="form-control"  id="">
                           <option value=" ">Select</option> 
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>                    
                       </select>
                      </div>
                    </div> 
                    <div class="col-sm-6 col-md-6"  >
                      <div class="form-group">
                        <label class="form-label">Select Date 3</label>                        
                         <select name="quterly_date3" class="form-control 1-31" id=""> 
                          <option value=" ">Select</option>                       
                       </select>
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6" >
                      <div class="form-group">
                        <label class="form-label">Select Month 3</label>
                         <select name="quterly_month3" class="form-control"  id="">
                           <option value=" ">Select</option> 
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>                    
                       </select>
                      </div>
                    </div> 
                    <div class="col-sm-6 col-md-6"  >
                      <div class="form-group">
                        <label class="form-label">Select Date 4</label>                          
                         <select name="quterly_date4" class="form-control 1-31"  id=""> 
                          <option value=" ">Select</option>                       
                       </select>
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6" >
                      <div class="form-group">
                        <label class="form-label">Select Month 4</label>
                         <select name="quterly_month4" class="form-control"  id="">
                            <option value=" ">Select</option> 
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>                    
                       </select>
                      </div>
                    </div> 
                    </div> 
                     <div class="row col-md-12" id="anually" style="display: none">
                    <div class="col-sm-6 col-md-6"  >
                      <div class="form-group">
                        <label class="form-label">Select Date</label>                         
                         <select name="annualy_date" class="form-control 1-31"  id="">    
                          <option value="">Select</option>                    
                       </select>
                      </div>
                    </div>  
                     <div class="col-sm-6 col-md-6" >
                      <div class="form-group">
                        <label class="form-label">Select Month</label>
                         <select name="annually_month" class="form-control" id="">
                           <option value=" ">Select</option> 
                          <option value="01">January</option>
                          <option value="02">February</option>
                          <option value="03">March</option>
                          <option value="04">April</option>
                          <option value="05">May</option>
                          <option value="06">June</option>
                          <option value="07">July</option>
                          <option value="08">August</option>
                          <option value="09">September</option>
                          <option value="10">October</option>
                          <option value="11">November</option>
                          <option value="12">December</option>                       
                       </select>
                      </div>
                    </div> 
                    </div>      
                     
                     
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>   
                <script type="text/javascript">
               $(function(){
    var $select = $(".1-31");
    for (i=1;i<=31;i++){
        $select.append($('<option></option>').val(i).html(i))
    }
});  
</script>           
                <script type="text/javascript">
$(document).ready(function(){
     $('#ddlPassport').on('change', function() {
      if ( this.value == 'Monthly' )
      {
        $("#month").show();
      }
      else
      {
        $("#month").hide();
        //$("#year").hide();
      }
      
      
      if ( this.value == 'Quarterly')
      {
        $("#quterly").show();
      }
      else
      {
        $("#quterly").hide();
        //$("#year").hide();
      }
      
       if ( this.value == 'Annually')
      {
        $("#anually").show();
      }
      else
      {
        $("#anually").hide();
        //$("#year").hide();
      }
       
    });
    
   
     
});

</script>



<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>