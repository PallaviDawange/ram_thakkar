<!doctype html>
<html lang="en" dir="ltr">
  <head>
<?php include('include/header_datatable.php'); ?>
  </head>
  <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
       
              <?php
            echo form_open('get_searched_data',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                     
                <div class="card">
                    <div class="sticky-top text-right py-4" style="padding-right:50px" > 
                      <a class="btn btn-info" style="width:160px" href="<?php echo base_url();?>add_registration">Add Entry</a>
                    </div>
                  
                       
                <div class="card-body">
                  <!-- <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6> -->
                  <h3 class="card-title" style="color:green; font-size:25px;"><b><b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label"> Company</label>
                        <select type="text" class="form-control search-select required"  value="" id="company_name">
                           <option value="">All</option>
                          <option value="inward">Aobs</option>
                           <option value="outward">Alpha</option>
                            <option value="outward">Tcs</option>
                        </select>
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Select Inward/Outward</label>
                        <select type="text" class="form-control"  value="" id="type">
                           <option value="">Select</option>
                          <option value="inward">Inward</option>
                           <option value="outward">Outward</option>
                        </select>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label"> Date From</label>
                        <input type="date" class="form-control"  value="" id="type">                        
                      </div>
                    </div>    
                      <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label"> Date To</label>
                        <input type="date" class="form-control"  value="" id="type">                        
                      </div>
                    </div>                                       
                                       
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Search</button>
            </div>                
               
              </form>
            </div>
          </div>
              
                  <!-- MODAL -->
            
<div class="modal fade show" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
      <div class="modal-content panel-info panel-color">
      
          <div class="modal-header panel-info panel-color">             
              <h4 class="modal-title" id="myModalLabel">Confirm Delete</h4>
               <!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
          </div>
      
          <div class="modal-body">
              <p>You are about to delete one record, this procedure is irreversible.</p>
              <p>Do you want to proceed?</p>
                 </div>
          
          <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <a class="btn btn-danger btn-ok">Delete</a>
          </div>
      </div>
  </div>
</div>
<script>
            //searchBox = document.querySelector("# ");
            countries = document.querySelector("#company_name");
            var when = "keyup"; //You can change this to keydown, keypress or change
            searchBox.addEventListener("keyup", function (e) {
            var text = e.target.value; 
            var options = countries.options; 
            for (var i = 0; i < options.length; i++) {
            var option = options[i]; 
            var optionText = option.text; 
            var lowerOptionText = optionText.toLowerCase();
            var lowerText = text.toLowerCase(); 
            var regex = new RegExp("^" + text, "i");
            var match = optionText.match(regex); 
            var contains = lowerOptionText.indexOf(lowerText) != -1;
            if (match || contains) {
            option.selected = true;
            return;
            }
            searchBox.selectedIndex = 0;
            }
            });
    </script>
    <script>
    $(function(){
         $('.search-select').searchableSelect();
    });
    </script>
<?php include "Ajax/customdelete.php"; ?>
                  <script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>
     <script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>        
                </body>
                </html>