<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
     <style>
#star{
    color: red;
    font-size:20px;
}
label.error{
    color: red;
    font-size: 12px;
}
  </style>
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>

              <?php
            echo form_open_multipart('save_company',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6>
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Add Company<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Name of Company<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="company_name">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Identification No.<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="identification_no">
                      </div>
                    </div>
                          <div class="col-md-12" id="field-increase">
                        <div class="row">
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                            <label class="form-label"> Director Name<span id="star">*</span></label>
                            <input type="text" class="form-control shippping_bill" name="director[]" required>
                            <span id="error4" style="display:none;color:red;"> </span>
                            </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                            <label class="form-label">Email Id.<span id="star">*</span></label>
                            <input class="form-control datepicker" type="text" name="email[]" id="payment_date" required>                                                       
                            </div>  
                            </div>
                            <div class="col-md-4 col-sm-12">
                            <div class="form-group">
                            <label class="form-label">Action</label>
                            <input type="button" class="btn btn-primary" id="addmore" value="Add More">
                            </div>
                            </div>
                        </div>
                    </div>
                
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Landline No.</label>
                        <input type="text" class="form-control" required="" value="" name="landline">
                      </div>
                    </div>
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Address<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="address">
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6 button dropdown">
                      <div class="form-group">
                        <label class="form-label">Mobile No<span id="star">*</span></label>
                        <input name="frequency" class="form-control" required="" id="mobile">
                         
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Remark</label>
                        <input type="text" class="form-control" required="" value="" name="remark">
                      </div>
                    </div>
                  

                   
                     
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>   
             <script>
    $(document).ready(function(){  
        $("#addmore").click(function(){  
            $("#field-increase").append(
                '<div class="row">'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<input type="text" class="form-control" name="director[]" required>'
                +'</div>'
                +'</div>'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<input class="form-control datepicker" type="text" name="email[]" id="payment_date" required>'                                                      
                +'</div>'
                +'</div>'
                +'<div class="col-md-4 col-sm-12">'
                +'<div class="form-group">'
                +'<button class="btn btn-danger" id="remove">Remove</button>'
                +'</div>'
                +'</div>'
                +'</div>'
            );  
             $(".datepicker").datepicker({
            format: 'dd/mm/yyyy',
             });
        });
        $(document).on("click","#remove",function(){
            $(this).closest('.row').remove();
        });
    });
    </script>



<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>