<script>                 
 function ViewOrder(id,prod_id){

        $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo base_url();?>Orders/view_order_details',
                data: 'id='+id +'&prod_id=' + prod_id,
                success: function(data) 
                {
                    $('#demo').html(data);
                }
            });
       }
</script>  
<script>                 
 function ViewPaymentProof(id,prod_id){
        $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo base_url();?>Orders/view_payment_details',
                data: 'id='+id +'&prod_id=' + prod_id,
                success: function(data) 
                {
                    $('#payment').html(data);
                }
            });
       }
</script>  

<script>                 
 function ViewCourierDetail(id,prod_id){
        $.ajax({
                cache: false,
                type: 'POST',
                url: '<?php echo base_url();?>Orders/view_courier_details',
                data: 'id='+id +'&prod_id=' + prod_id,
                success: function(data) 
                {
                    $('#courier').html(data);
                }
            });
       }
</script> 
<script>
        var check = function() {
        var y = document.getElementById("eseal_orderd").value;
        var a=y%10;
        if (a ==0)
        {
        document.getElementById('message').style.color = 'green';
        document.getElementById('message').innerHTML = '';
        } else {
        document.getElementById('message').style.color = 'red';
        document.getElementById('message').innerHTML = 'Please enter value in multiple of 10';
        }
        }
 </script>
 <script>
 function ViewDate(){
      $(document).ready(function(){
      var date_input=$('input[name="payment_recv_date"]'); 
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'dd/mm/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        }
      var todays = dd + '/' + mm + '/' + yyyy;
       document.getElementById('payment_date').value=(todays);
      date_input.datepicker(options);
 
 });
 }
 </script>
  <script>
        $(".datepicker").datepicker({
            format: 'dd/mm/yyyy',
        });
    </script>
 
<script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
</script>
<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>
    
<link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>