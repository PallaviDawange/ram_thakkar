<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
   
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
              <?php
            echo form_open_multipart('get_client_wise_report',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Staff Wise Report<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-12 col-md-12 ">
                      <div class="col-md-6">
                      <div class="form-group mx-auto">
                        <label class="form-label">Staff Name</label>
                        <select class="form-control" name="client_name">
                          <?php if(count($posts) > 0) {
                                foreach( $posts as $post ){
                        ?>
                          <option value="<?php echo $post->client_id;?>"><?php echo $post->client_name;?></option>
                          <?php } }?>
                        </select>
                      </div>
                      </div>
                    </div>
                     <div class="col-md-2"></div>
                      
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Date From</label>
                        <input type="date" name="from" class="form-control">
                      </div>
                      </div>

                      <div class="col-md-4">
                      <div class="form-group">
                        <label class="form-label">Date To</label>
                        <input type="date" name="to" class="form-control">
                      </div>
                      </div>
                      <div class="col-md-2"></div>            
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>                  
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>                
                

<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>