<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
   
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
              <?php
            echo form_open_multipart('save_client',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Add client<b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Client Name</label>
                        <input type="text" class="form-control"  value="" name="client_name" id="client_name" >
                        <span id="errorrequired1" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Company Name</label>
                        <input type="text"  class="form-control"  value="" name="company_name" id="company_name" >
                        <span id="errorrequired2" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">GST Number</label>
                        <input type="text" class="form-control gstinnumber"  value="" name="gst" id="gst">
                         <span id="error3" style="display:none;color:red;">Please Enter Valid GST Number</span>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Location</label>
                        <input type="text" class="form-control" value="" name="location" id="location" >
                        <span id="errorrequired3" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                      
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Mobile</label>
                        <input type="text" class="form-control" value="" name="mobile" id="mobileNo" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');"  maxlength="10" required>
                     <span id="error2" style="display:none;color:red;">Mobile No. must be 10 digit</span>
                      </div>
                    </div>
                      <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Email</label>
                        <input type="email" class="form-control" value="" name="email" id="email_address" onkeypress="validateEmail(this.value);" required>
                      <span id="error" style="display:none;color:red;">A valid email address is required</span>
                      </div>
                    </div>
                   
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Address</label>
                        <textarea type="text" class="form-control"  name="address" rows="3"></textarea>
                      </div>
                    </div>                                      
                               
            <div class="card-footer text-center">
              <button type="submit" name="save" id="demo1" class="btn btn-success">Submit</button>                  
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>                
                

<script>
$('#demo1').click(function(){
         var emailReg = /^([\w- \.]+@([\w-]+\.)+[\w-]{2,4})?$/;
         var number=document.getElementById("mobileNo").value;       
        
         var email=$('#email_address').val()
          //var email=document.getElementById("client_name").value;
         var client_name=document.getElementById("client_name").value;
         var company_name=document.getElementById("company_name").value;
         var gst=document.getElementById("gst").value;
         var location=document.getElementById("location").value;
        
        if((number.length==10)  && (gst.length ==15) ){
             if(client_name==""){
            //$('#myForm').submit();
            $('#errorrequired1').show();
           } else {
              $('#errorrequired1').hide();
             if(company_name==""){
            $('#errorrequired2').show();
             } else {
              $('#errorrequired2').hide(); 
           
             if(location==""){
            $('#errorrequired3').show();
             } else {
              $('#errorrequired3').hide();           
              $('#myForm').submit();
           
            
           }}}  
        }  else {
        
      if(number.length!=10){
          $('#error2').show();
      }  else {
          $('#error2').hide();
      }
     
      if(gst.length !=15){
          $('#error3').show();
      } else {
          $('#error3').hide();
      }
    
}
    
});

function validateEmail(email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    if (!emailReg.test(email)) {
        $('#error').show();
    } else {
       $('#error').hide();
        
    }
}


// function validateNumber(number) {
//   // alert(number);
//     if(number.length!=10) {
//         $('#error2').show();
//     } else {
//       $('#error2').hide();
        
//     }
// }

</script>
<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

     <script> 
        $(document).on('change',".gstinnumber", function(){    
        var inputvalues = $(this).val();
        var gstinformat = new RegExp('^[0-2]{1}[0-9]{1}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$');
        if (gstinformat.test(inputvalues)) {
        //  return true;
        $('#error2').hide();
        } else {
        $('#error2').show();
        // $(".gstinnumber").focus();
        }
        
        });
    </script>

    </html>