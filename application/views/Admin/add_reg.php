<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
     <style>
#star{
    color: red;
    font-size:20px;
}
label.error{
    color: red;
    font-size: 12px;
}
  </style>
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
          <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>

              <?php
            echo form_open_multipart('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6>
                  <h3 class="card-title" style="color:green; font-size:25px;"><b><b></h3>
                  <div class="row" align="center">      
                      
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Select Inward/Outward<span id="star">*</span></label>
                        <select type="text" class="form-control" required="" value="" id="type">
                           <option value="">Select</option>
                          <option value="inward">Inward</option>
                           <option value="outward">Outward</option>
                        </select>
                      </div>
                    </div>
                 
                  <div class="col-sm-12 col-md-12" id="in" style="display: none">
                
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Date Received<span id="star">*</span></label>
                        <input type="date" class="form-control" required="" value="" name="designation">
                      </div>
                    </div>
                  </div>
                  <div class="col-sm-12 col-md-12" id="out" style="display: none">
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Send Date<span id="star">*</span></label>
                        <input type="date" class="form-control" required="" value="" name="address">
                      </div>
                    </div>
                  </div>
                   
                    <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Company Name<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="username">
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Party Name<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="password">
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Courier Name<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="password">
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Document Name<span id="star">*</span></label>
                        <input type="text" class="form-control" required="" value="" name="password">
                      </div>
                    </div>
                     <div class="col-sm-12 col-md-12">
                      <div class="form-group">
                        <label class="form-label">Remark</label>
                        <textarea type="text" class="form-control" required="" value="" name="password"></textarea>
                      </div>
                    </div>
                        
                   
                     
                               
            <div class="card-footer col-md-12 text-center">
              <button type="submit" name="save" class="btn btn-success">Submit</button>
            </div>                
               
              </form>
               </div> </div>
              </div>
               </div>   
             
<script type="text/javascript">
$(document).ready(function(){
     $('#type').on('change', function() {
      if ( this.value == 'inward' )
      {
        $("#in").show();
      }
      else
      {
        $("#in").hide();
        //$("#year").hide();
      }
      
      
      if ( this.value == 'outward')
      {
        $("#out").show();
      }
      else
      {
        $("#out").hide();
        //$("#year").hide();
      }
      
       
       
    });
    
   
     
});

</script>


<script> 
        setTimeout(function() {
            $('#flashdivs').hide('fast');
        }, 4000);
    </script>

    </html>