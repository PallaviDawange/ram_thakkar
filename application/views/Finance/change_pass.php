
    <?php include('include/headers.php'); ?>
  </head>
   <?php include('include/nav.php'); ?>
  <body class="" >
    <div class="page" >
      <div class="page-single">
        <div class="container" >
          <div class="row">
            <div class="col col-login mx-auto">
              <div class="text-center mb-6">
              </div>
              
               <?php
            echo form_open('update_password',array('class'=>"form-horizontal m-t-20 card" ,'id' => "loginForm",'name'=>"loginForm"));
                      ?>
                <div class="card-body p-6" >
                  <div class="card-title"><b>Change Password</b></div>
                  <div class="form-group">
                    <label class="form-label">Current Password</label>
                    <input type="password" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter your Current Password" name="password" required>
                  </div>
                  <div class="form-group">
                    <label class="form-label">New Password </label>
                    <input type="password" class="form-control" id="password" placeholder="Enter your New Password" name="confirm_pass" onkeyup='check();' required>
                  </div>
                    <div class="form-group">
                    <label class="form-label">New Password </label>
                    <input type="password" class="form-control" id="confirm_password" placeholder="Re-enter your New Password" name="confirm_password" onkeyup='check();' required>
                     <span id='message'></span>
                  </div>
                  <div class="form-footer">
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Submit</button>
                  </div>
                </div>
              </form>
             
            </div>
          </div>
        </div>
      </div>
    </div>
      <?php include('Ajax/pass_match.php'); ?>
  </body>
</html>