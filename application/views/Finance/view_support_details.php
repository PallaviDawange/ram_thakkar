<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/header_datatable.php'); ?>
   
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8" style="margin-top:25px;">
        <center>
              <?php
            echo form_open_multipart('save_agent_data',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>All  Details <b></h3>
                  <div class="row" align="center">
                      <?php foreach($support_detail as $row)
                                       {  
                                         
                                        ?>      
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Exporter Name</label>
                        <input type="text" class="form-control"  value="<?php echo $row->exporter_name; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Ticket Number</label>
                        <input type="text"  class="form-control"  value="<?php echo $row->TICKET_NO; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Phone Number</label>
                        <input type="text" class="form-control"  value="<?php echo $row->PHONE_NO; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Email ID</label>
                        <input type="text" class="form-control" value="<?php echo $row->exp_email; ?>" disabled>
                      </div>
                    </div>
                   
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Contact Person Name</label>
                        <input type="text" class="form-control" value="<?php echo $row->CONTACT_PERSON_NAME; ?>" disabled>
                      </div>
                    </div>
                   
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Subject</label>
                        <input type="text" class="form-control"  value="<?php echo $row->SUBJECT; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Priority</label>
                        <input type="text" class="form-control"  value="<?php echo $row->PRIORITY; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Date </label>
                        <input type="text" class="form-control" value="<?php echo $row->DATE_CREATED; ?>" disabled>
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-label">Description </label>
                        <textarea type="text" class="form-control" rows="5"  disabled><?php echo $row->DESCRIPTION; ?></textarea>
                      </div>
                    </div>
                  </div>
                  <?php } ?>
                </div>
               
            
              </form>
              </div>
<script>
    $('input:file').change(
    function(e){
    console.log(e.target.files[0].name);
    });
</script>

<script>
    $('#myForm').submit(function(e) {
    e.preventDefault();
    if(!$('#mobile').val().match('[0-9]{10}'))  {
    alert("Please put 10 digit mobile number");
    return;
    }  
    
    });
</script>
    </html>