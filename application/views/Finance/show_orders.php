<!doctype html>
<html lang="en" dir="ltr">
  <head>
<?php include('include/header_datatable.php'); ?>

<style>
.btn-space {
    margin-left: 5px;
}
.action-space {
    text-align:center;
}
.button {
    background-color: #dd0244; /* blue */
    border: none;
    color: white;
    padding: 8px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border-radius: 3px;
   
}
.button2 {
    background-color: #693894; /* silver */
    border: none;
    color: white;
    padding: 8px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border-radius: 3px;
   
}

.button3 {
    background-color: #693894; /* silver */
    border: none;
    color: white;
    padding: 8px 15px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    border-radius: 3px;
   
}
</style>
  </head>
  <?php include('include/nav.php'); ?>
  <body>
 <div class="container" style="margin:20px auto">
      <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
                <?php if($error = $this->session->flashdata('error_flash')): ?>
                  <div class="row">
                    <div class="col-lg-12">
                    <div class="alert alert-dismissible alert-danger">
                    <?= $error ?>
                    </div>
                    </div>
                    </div>
                    <?php endif; ?>
                <div class="card">
                    
                    <input type="hidden"><br>
                    <?php
            echo form_open('Orders/search_data',array('class'=>"form-horizontal m-t-20 " ,'id' => "myForm",'name'=>"myForm"));
                      ?>
        <div class="row" align="center" style="margin-left:20px;">
            <div class="col-md-4">
                    <div class="form-group">
                        <!--<div class="col-md-6">-->
                        <input type="text" class="form-control" name="order_no" placeholder="Search by Order No." >
                        </div> 
                        </div>
                         <div class="col-md-1">
                        <input type="submit" class="button2" value="Search" >
                        </div>                        
                    </div>
                    </form>
                  <table class=" table card-table table-vcenter " id="myTable1">
                      <thead>
                    <tr>
                     <th><b>Exporter Name</b></th>
                      <th><b>Order Through</b></th>
                      <th><b>Order Qty.</b></th>
                      <th><b>Date of Order</b></th>
                      <th><b>Proforma Number</b></th>
                      <th><b> Email Status</b></th>
                      <th><b>Proforma Status</b></th>
                       <th><b>Payment Proof</b></th>
                      <!--<th><b>Courier Details</b></th>-->
                      <th><b>View More Details</b></th>
                    </tr>
                    </thead>
                    <tbody id="table1">
                        <?php if(count($orders) > 0)
                                         {  
                                        $i=1;
                                        foreach($orders as $row)
                                       {  
                                         $originalDate = $row->PURCHASE_ORDER_DATE;
                                         $newDate = date("d/m/Y", strtotime($originalDate));
                                         //print_r($originalDate);
                                         $encrypted_order_id = $this->encrypt->encode($row->ORDER_NO);
                                         $encrypted_order_id = strtr($encrypted_order_id,array('+' => '.', '=' => '-', '/' => '~'));
                                        ?>                    
                    <tr>
                      <td><?php echo $row->exporter_name;?></td>
                      <td><?php echo $row->ORDER_THROUGH;?></td>
                      <td><?php if($row->QTY_TO_ALLOCATE_THIS_TIME=="0"){ echo $row->ORDER_QTY; } else {echo $row->QTY_TO_ALLOCATE_THIS_TIME;}?></td>
                      <td><?php echo $originalDate;?></td>
                      <td><?php echo $row->ORDER_NO;?></td>
                      <td class="text-center"><?php if($row->ORDER_STATUS == "Proforma_generated"|| $row->PAYMENT_RECEIVED_STATUS == "Payment_received"){ ?> 
                      <span class="text-success">
                      <i class="fa fa-check-square-o fa-ico"></i> </span>
                      <?php } else{  ?>
                       <span class="text-danger">
                      <i class="fa fa-close fa-ico"></i> </span>
                      <?php } ?></td>
                      <td><?php if($row->ORDER_STATUS == "Pending"){ ?> 
                      <a class="btn btn-primary btn-block"  data-toggle="modal" href="#proforma" onclick='ViewOrder(<?php echo $row->O_ID; ?>,<?php echo $prod_id; ?>);' ><font color="white">Generate Proforma</font></a> 
                      <?php }
                      elseif($row->ORDER_STATUS == "Proforma_requested"){ ?> <a class="button button2 btn-block"  data-toggle="modal" data-target="#proforma" onclick='ViewOrder(<?php echo $row->O_ID; ?>,<?php echo $prod_id; ?>);' ><font color="white">Approve Proforma </font> </a> <?php }
                      elseif($row->ORDER_STATUS == "cancelled"){ ?> <a class="btn btn-warning " style="padding-right:25px;padding-left:25px;" ><font color="white">Cancelled Order </font> </a> <?php }
                      else{  ?>
                        <a class="btn bg-lightwine btn-block"><font color="white">Proforma Generated</font></button>
                      <?php }  ?></td>
                      <td><?php if($row->PAYMENT_PROOF==""){  ?>  <?php } else { ?><a class="btn btn-success btn-block" data-toggle="modal" href="#paymentproof" onclick='ViewPaymentProof(<?php echo $row->O_ID; ?>,<?php echo $prod_id; ?>);' target="_blank" data-toggle="tooltip" title="View Payment Proof"><font color="white">Click to View</font></button> <?php } ?></td>
                      <!--<td><?php //if($row->COURIER_DETAIL==""){  ?>  <?php //} else { ?><a class="btn btn-info btn-block" data-toggle="modal" href="#courier_detail" onclick='ViewCourierDetail(<?php echo $row->O_ID; ?>,<?php echo $prod_id; ?>);' target="_blank" data-toggle="tooltip" title="View Courier Details"><font color="white">View</font></button><?php //} ?></td>-->
                      <td class="text-center"><a href="<?php echo base_url();?>view_order_details/<?php echo $encrypted_order_id;?>"  class="on-default edit-row txt-brown p-3" data-toggle="tooltip" title="View more Details"><i class="fa fa-eye fa-ico"></i></a> </td>
                    </tr>
                    
                      <?php   }} ?>
                     </tbody>
                  </table>
                   <div><?php echo $links; ?></div>
                 
                  <!-- modal start -->
<div class="modal fade show" id="proforma" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold" style="color:orange";>Generate Proforma </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <?php
            echo form_open('generate_proforma',array('class'=>'form-horizontal m-t-20 card' ,'id' => 'myform5','name'=>'myform' ));
                      ?>
             <!--<form action="generate_proforma" id="myform5"  class="form-horizontal m-t-20 card">-->
            <div class="modal-body mx-6">
                <div id="demo"></div>
            </div>
            <!--<div class="modal-footer d-flex justify-content-center">-->
            <!--    <span id='errors1' style='display:none;color:red;'>E-seals rate and commission has to be 300 minimum</span>-->
                <!--<input type="button" class="btn btn-success" id="demo1" value="Generate Proforma">-->
                <!--<input type="text" value="asd" name="asd">-->
                <!--<button class="btn btn-success" type="submit">Generate Proforma </button>-->
            <!--</div>-->
            </form>
        </div>
    </div>
</div>
<!-- modal end -->

         <!-- modal finance proforma -->
<div class="modal fade show" id="proforma1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold" style="color:orange";>Generate Proforma </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <?php
            echo form_open('generate_proforma',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myform",'name'=>"myform"));
                      ?>
            <div class="modal-body mx-6">
                <div id="demo"></div>
            </div>
            <div class="modal-footer d-flex justify-content-center"> 
            <input type="button" class="btn btn-success" id="" value="Generate Proforma">
                <input class="btn btn-success" type="submit">Generate Proforma </button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- modal end -->

        <!-- modal Payment proof -->
<div class="modal fade show" id="paymentproof" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold" style="color:orange";>Payment Proof </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <?php
            echo form_open('update_payment_status',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myform",'name'=>"myform"));
                      ?>
            <div class="modal-body mx-6">
                <div id="payment"></div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-success" type="submit" id="submitbutton" >Submit </button>
            </div>
 
            </form>
        </div>
    </div>
</div>
<!-- modal end -->

<!-- modal Courier Detail -->
<div class="modal fade show" id="courier_detail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold" style="color:orange";>Courier Detail </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                </button>
            </div>
            <?php
            echo form_open('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myform",'name'=>"myform"));
                      ?>
            <div class="modal-body mx-6">
                <div id="courier"></div>
            </div>
            <div class="modal-footer d-flex justify-content-center">
                <button class="btn btn-success" type="submit">Submit </button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- modal end -->

    
<?php include ('Ajax/show_order_script.php'); ?>

</body>
</html>