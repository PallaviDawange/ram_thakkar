<!doctype html>
<html lang="en" dir="ltr">
  <head>
<?php include('include/header_datatable.php'); ?>
  </head>
  <?php include('include/nav.php'); ?>
  <body>
   <div class="container" style="margin:20px auto">
       
              <?php
            echo form_open('',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                     
                <div class="card">
                  <table class="table card-table table-vcenter" id="myTable">
                      <thead>
                    <tr>
                     
                     <th><b>Sr. No.</b></th>
                      <th><b>Agent Name</b></th>
                      <th><b>Email ID</b></th>
                      <th><b>Mobile Number</b></th>
                       <th><b>Company Name</b></th>
                       <th><b>View Details</b></th>
                     
                    </tr>
                    </thead>
                    <tbody id="table1">
                        <?php if(count($hosts) > 0)
                                         {  
                                             $i=1;
                                           foreach($hosts as $row)
                                       { 
                                         $encrypted_agent_id = $this->encrypt->encode($row->agent_id);
                                         $encrypted_agent_id = strtr($encrypted_agent_id,array('+' => '.', '=' => '-', '/' => '~'));
                                         
                                        ?>                    
                    <tr>
                      <td><?php echo $i++;?></td>
                       <td><?php echo $row->agent_name;?></td>
                      <td><?php echo $row->agent_email;?></td>
                      <td><?php echo $row->agent_mobile;?></td>
                      <td><?php echo $row->organization_name;?></td>
                       <td><a href="<?php echo base_url();?>view_agent_details/<?php echo $encrypted_agent_id;?>" style="color:#00BFFF;font-size:20px;"  title="View more Details" target="_blank"><i class="fa fa-eye"></i></a> </td>   
                    </tr>
                    
                      <?php   }} ?>
                     </tbody>
                  </table>
     <script>
$(document).ready(function(){
    $('#myTable').dataTable();
});
$('#myTable').dataTable( {
  "ordering": false
} );
</script>        
                </body>
                </html>