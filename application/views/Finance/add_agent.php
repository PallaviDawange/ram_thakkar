<!doctype html>
<html lang="en" dir="ltr">
  <head>
    <?php include('include/headers.php'); ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<style>
 #star {
    color: red;
    font-size:20px;
}
 </style>
  </head>
   <?php include('include/nav.php'); ?>
 <div align="center">
    <div class="col-lg-8">
        <center>
              <?php
            echo form_open_multipart('save_agent_data',array('class'=>"form-horizontal m-t-20 card" ,'id' => "myForm",'name'=>"myForm"));
                      ?>
                <div class="card-body">
                      <div id="flashdivs">   
                <?php  echo $this->session->flashdata('msg'); ?> 
                </div>
                <p>This is test by pallavi</p>
                  <h3 class="card-title" style="color:green; font-size:25px;"><b>Register a New Agent <b></h3>
                  <h6 style="color:red;"> All fields mark in (*) are mandatory.</h6>
                   <span  style="color:#6272af;">Note : It is important to enter Email ID carefully as it will be used for all communications from mindeseals.<br></span>
                  <div class="row" align="center">
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Contact Person Name <span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter Contact Person Name" value="" name="agent_name" id="exp_name" >
                        <span id="errorrequired" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Contact Mobile Number <span id="star">*</span></label>
                        <input type="text"   name="mobile_no" id="mobileNo" class="form-control"   placeholder="Enter Mobile Number"  name="mob" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="10" >
                        <span id="error2" style="display:none;color:red;">Mobile No. must be 10 digit</span>
                      </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Email ID <span id="star">*</span></label>
                        <input type="email" class="form-control" placeholder="Enter Email ID" value="" name="email" id="email_address" onkeypress="validateEmail(this.value);" >
                        <span id="error" style="display:none;color:red;">A valid email address is required</span>
                    </div>
                    </div>
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">PAN Number<span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter PAN Number" value=""  name="pan_no" id="pan_nos" required>
                        <span id="error5" style="display:none;color:red;">Enter Valid PAN Number</span>
                      </div>
                    </div>
                    <!--  <div class="col-sm-6 col-md-6">-->
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label">IEC <span id="star">*</span></label>-->
                    <!--    <input type="text" class="form-control" placeholder="Enter IEC" value="" name="Iec_no" id="iec" required>-->
                    <!--    <span id="errorrequired6" style="display:none;color:red;">Please fill out this field</span>-->
                    <!--  </div>-->
                    <!--</div>-->
                      <div class="col-sm-6 col-sm-6">
                      <div class="form-group">
                        <label class="form-label">Organization Name <span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter Organization Name" value="" name="organization_name" id="organization_name" >
                        <span id="errorrequired3" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                    <!--<div class="col-sm-6 col-md-6">-->
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label">Billing Address <span id="star">*</span></label>-->
                    <!--    <textarea rows="3" class="form-control" placeholder="Enter Billing Address" name="billing_address" id="billing_address" ></textarea>-->
                    <!--    <span id="errorrequired4" style="display:none;color:red;">Please fill out this field</span>-->
                    <!--  </div>-->
                    <!--</div>-->
                    <!--<div class="col-sm-6 col-md-6">-->
                      
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label"> Shipping / Delivery Address <span id="star">*</span></label>-->
                    <!--    <textarea rows="3" class="form-control" placeholder="Enter Shipping / Delivery Address" name="shipping_address" id="shipping_address" ></textarea>-->
                    <!--    <span id="errorrequired5" style="display:none;color:red;">Please fill out this field</span>-->
                    <!--  </div>-->
                    <!--</div>-->
                    <!--<div class="col-sm-6 col-md-6">-->
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label">Billing Address Pincode<span id="star">*</span></label>-->
                    <!--    <input type="text" class="form-control" placeholder="Enter Billing Address Pincode" value="" name="billing_pincode" id="billing_pincode" maxlength="6" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>-->
                    <!--  <span id="error7" style="display:none;color:red;">Enter Valid Pincode </span>-->
                      
                    <!--  </div>-->
                    <!--</div>-->
                    <!-- <div class="col-sm-6 col-md-6">-->
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label">Shipping Address Pincode<span id="star">*</span></label>-->
                    <!--    <input type="text" class="form-control" placeholder="Enter Shipping Address Pincode" value="" name="shipping_pincode" id="shipping_pincode" maxlength="6" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" required>-->
                    <!--    <span id="error8" style="display:none;color:red;">Enter Valid Pincode </span>-->
                    <!--  </div>-->
                    <!--</div>-->
                     
                    <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label"> GST Address<span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter GST Address" value=""  name="cst_no" id="cst_nos" >
                        <!--<span id="errorrequired2" style="display:none;color:red;">Enter Valid CST Number</span>-->
                        
                      </div>
                    </div>
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">GSTN<span id="star">*</span></label>
                        <input type="text" class="form-control gst_cls" placeholder="Enter Billing GSTN" value="" name="gst_no" id="gst_nos" onchange="test_gst_state(this.value);"  required>
                         <span id="error4" style="display:none;color:red;">Enter valid GST Number.</span>
                      </div>
                    </div>
                  
                    
                     <div class="col-sm-6 col-md-6">
                      <div class="form-group">
                        <label class="form-label">Office Contact Number <span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter Office Contact Number" name="office_contact_no" id="mobile2"   oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" maxlength="10" >
                      <span id="error3" style="display:none;color:red;">Mobile No. must be 10 digit</span>
                      </div>
                    </div>
                    <!-- <div class="col-sm-6 col-sm-6">-->
                    <!--  <div class="form-group">-->
                    <!--    <label class="form-label">Location Name <span id="star">*</span></label>-->
                    <!--    <input type="text" class="form-control" placeholder="Enter Location Name" value="" name="location_name" id="location" required>-->
                    <!--    <span id="errorrequiredss" style="display:none;color:red;">Please fill out this field</span>-->
                    <!--  </div>-->
                    <!--</div>-->
                    
                     <div class="col-sm-6 col-sm-6">
                      <div class="form-group">
                        <label class="form-label">State <span id="star">*</span></label>
                        <!--<input type="text" class="form-control state_cls" placeholder="Enter State Name" value="" name="state_name" id="state">-->
                        <select type="text" class="form-control state_cls" name="state_name" id="state">
                         <option value="">Select</option>
                        <?php if(count($states) > 0)
                        {
                        foreach($states as $state)
                        {?>
                        <option value="<?php echo $state->StateName;?>"><?php echo $state->StateName;?></option>
                        <?php } 
                        } 
                        ?>
                        </select>
                        
                        <span id="errorrequired7" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                     <div class="col-sm-6 col-sm-6">
                      <div class="form-group">
                        <label class="form-label">City <span id="star">*</span></label>
                        <input type="text" class="form-control" placeholder="Enter City Name" value="" name="city_name" id="city">
                        <span id="errorrequired8" style="display:none;color:red;">Please fill out this field</span>
                      </div>
                    </div>
                   
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-label">Attach copy of GST Certificate</label>
                        <input type="file" class="form-control" name="userfile_1" >
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-label">Attach copy of IEC Certificate</label>
                        <input type="file" class="form-control" name="userfile_2">
                      </div>
                    </div>
                    <div class="col-md-12">
                      <div class="form-group">
                        <label class="form-label">Attach copy of PAN Card</label>
                        <input type="file" class="form-control" name="userfile_3">
                      </div>
                    </div>
                    
                  </div>
                </div>
                <div class="card-footer text-center">
                  <input type="button"  class="btn btn-success" id="demo1" value="Submit">
                </div>
            
              </form>
              </div>
              
               
<?php include ('Ajax/agent_validation.php'); ?>
<?php include ('Ajax/get_state.php'); ?>

    </html>