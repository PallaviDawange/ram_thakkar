<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Staff_management extends CI_Controller {

	 public function index()
           {             
             $this->load->view('Admin/staff_login');        
          } 

           public function client_otp()
           {             
             $this->load->view('Admin/client_otp');        
          } 


           public function Staff_validate()
                            { 

                                 redirect('staff_dashboard');
                                                           // print_r('form_validation') ;exit();   
                                // $this->load->library('form_validation');
                                // $this->form_validation->set_rules('username','username','required');
                                // $this->form_validation->set_rules('password','password','required');
                            
                                // if($this->form_validation->run() == TRUE)
                                // {
                                //     $password=$this->input->post('password');
                                //     $username=$this->input->post('username');                                                     
                                     //$check_mobile=$this->Client_manage_model->check_client_mobile($mobile);              
                                     //$pass=$check_mobile->password;   
                                     //$check_pass=$this->Client_manage_model->check_client_password($mobile,$password);
                                    // print_r($check_pass);exit();
                                     //if ($check_pass==TRUE) {
                                          //$this->session->set_flashdata('msg', '<div class="alert alert-success">OTP has been sent to your mobile,Please use to Login </div>');
                                       // $this->session->set_userdata('admin_id',$check_pass);
                                        //redirect('staff_dashboard');
                                //      }else{
                                //          $this->session->set_flashdata('login_failed','Something went wrong,Please try again');
                                //        return redirect('client'); 
                                //      }
                                                    
                                // } else{
                                //      $this->session->set_flashdata('login_failed','Please Enter Password');
                                //        return redirect('client'); 

                                // }
                            
                           // }
                        }


                    public function validate_otp()
                            { 
                           
                            $otp=$this->input->post('otp'); 
                            $check_otp=$this->Client_manage_model->check_client_otp($otp);
                             if ($check_otp) { 
                                $this->session->set_userdata('admin_id',$check_otp);
                                redirect('client_dashboard');
                           }
                            else
                              {
                                $this->session->set_flashdata('login_failed','Something went wrong,Please try again');
                               return redirect('otp');
                             }
                         }

                     public function staff_dashboard()
                        {
                           //  $client_id=$this->session->userdata('admin_id');
                           //  $id=$client_id->client_id;                            
                           // $data['posts'] = $this->Client_manage_model->show_logged_client_data($id);
                           // $data['client_services'] = $this->Client_manage_model->show_client_services($id);   
                       // print_r($data);exit();          
                        $this->load->view('Admin/staff_dashboard');        
                        } 
                            
                    

                        public function sendOtp($mobile,$otp)
                                    {
                                    $apikey = "";
                                $test="http://login.absl.net.in/api/mt/SendSMS?user=alphaobs&password=alpha@21&senderid=ALPOBS&channel=Trans&DCS=8&flashsms=0&number=$mobile&text=Dear%20Client%20This%20is%20your%20OTP%20use%20use%20it%20to%20login%20$otp&route=34";                                       
                                    $curl = curl_init();
                                    //Set some options - we are passing in a useragent too here
                                    curl_setopt($curl, CURLOPT_URL, $test);
                                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                                    //Send the request & save response to $resp
                                    $resp = curl_exec($curl);
                                    curl_close($curl);
                                    return $resp;
                                    
                                    }

            
    
     public function show_client()
           {  
             $data['client_data'] = $this->Client_manage_model->show_client_data();
             $this->load->view('Admin/show_client',$data);
        
          } 
          public function add_client()
           {             
             $this->load->view('Admin/add_client');
        
          } 

           public function save_client()
           {   
           //print_r($_POST) ;exit();         
             					$post = $this->input->post();                            
                               
                                $array = array(
                                'client_name' =>$post['client_name'],
                                'company' =>$post['company_name'],
                                'gst_no' =>$post['gst'],
                                'location' =>$post['location'],                                
                                'mobile' =>$post['mobile'],
                                'email' =>$post['email'],
                                'address' =>$post['address'],                                
                                );                              
                               
                                $inserted = $this->Client_manage_model->save_client_data($array);                             
                                
                                if ($inserted == TRUE)
                                {                               
                                    $this->session->set_flashdata('msg', '<div class="alert alert-success">You have added Client Successfully</div>');
                                    redirect('show_client');
                               
                                } else {
                                 $this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                                    redirect('show_client');
                                }
        
         	 } 

		public function edit_client_details()
			{ 
						$seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $client_id= $this->encrypt->decode($seg_id);						
					    $data['client_data'] = $this->Client_manage_model->edit_client_data($client_id);   
					   // print_r($data);exit();    
						$this->load->view('Admin/edit_client',$data);
			} 

						 public function update_client() {   
           //print_r($_POST) ;exit();         
             					$post = $this->input->post();  
             					$hidd_id = $this->input->post('id');                      
                               
                                $array = array(
                                'client_name' =>$post['client_name'],
                                'company' =>$post['company_name'],
                                'gst_no' =>$post['gst'],
                                'location' =>$post['location'],                                
                                'mobile' =>$post['mobile'],
                                 'email' =>$post['email'],
                                'address' =>$post['address'],                                
                                );                              
                               
                                $updated = $this->Client_manage_model->update_client($array,$hidd_id);                           
                                
                                if ($updated == TRUE)
                                {                               
                                    $this->session->set_flashdata('msg', '<div class="alert alert-success">You have Updated Client Data Successfully</div>');
                                    redirect('show_client');
                               
                                } else {
                                 $this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                                    redirect('show_client');
                                }
        
         	 } 

         	 public function delete_client_details()
						{ 
						$seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $client_id= $this->encrypt->decode($seg_id);	
	                  //  print_r($client_id)	;exit();				
					    $deleted = $this->Client_manage_model->delete_client($client_id);   
					    if ($deleted == TRUE)
                                {                               
                                    $this->session->set_flashdata('msg', '<div class="alert alert-success">You have Deleted Client Data Successfully</div>');
                                    redirect('show_client');
                               
                                } else {
                                 $this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                                    redirect('show_client');
                                }
					 
						} 

                    public function assign_client_service()
                        { 
                       //  $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
                       //  $client_id = $this->encrypt->decode($seg_id);
                       // // print_r($client_id);exit(); 
                       // $data['c_id'] =  $client_id;                     
                       //  $data['services'] = $this->Client_manage_model->get_services();
                       //  $data['selected'] = $this->Client_manage_model->get_selected($client_id);
                       //  //print_r($data);exit();    
                        $this->load->view('Admin/add_client_service');
                        }

                    public function save_client_service()
                    {
                        $date=date('d/m/Y');
                        $post = $this->input->post();
                        $client_id = $post['client_id'];
                        $service_id = $post['service_name'];
                        //print_r($service_id);exit();
                        $deleted = $this->Client_manage_model->deleted_previous($client_id);
                        foreach( $service_id as $s_id ){
                            //print_r($s_id);exit();
                            $array = array(
                            'client_id' => $client_id,
                            'sv_id' => $s_id,
                            'service_date' => $date
                            );
                            //print_r($array);exit();
                        
                        
                        $insert = $this->Client_manage_model->store_services($array);
                    }
                        $this->session->set_flashdata('msg', '<div class="alert alert-success">You have added Client Services Successfully</div>');
                        redirect('show_client');
                    }

                    public function show_client_dashboard()
                    {
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
                        $client_id = $this->encrypt->decode($seg_id);
                        $data['posts'] = $this->Client_manage_model->get_one_client($client_id);
                        $data['ghosts'] = $this->Client_manage_model->getClientServices($client_id );
                        //print_r($data['ghosts']);exit();
                        $this->load->view('Admin/dashboard',$data);
                    } 

                    public function update_service_status()
                    {
                        //print_r($_POST);exit();
                        $post = $this->input->post();
                        //print_r($post['status']);exit();
                        $id = $post['id'];
                        $date_now = date('d/m/Y');
                        if( $post['status'] == "completed"){
                            $comp="completed";
                            $array = array(
                            'status' => $comp,
                            'completed_date' => $date_now,
                            );
                        $updated = $this->Client_manage_model->update_status($array,$id);

                        }
                        elseif( $post['status'] == "missing"){
                            $miss="missing";
                            $array = array(
                            'status' => $miss,
                            'missing_date' => $date_now,
                            );
                        $updated = $this->Client_manage_model->update_status($array,$id);
                        //print_r('c');exit();
                        }
                        /*if( $post['status'] == 'incorrect'){
                            $array = array(
                            'status' => $post['status'],
                            'incorrect_date' => $date_now,
                            );
                        $updated = $this->Client_manage_model->update_status($array,$id);
                        }*/
                        elseif( $post['status'] == "processing"){
                            $pro="processing";
                            $array = array(
                            'status' => $pro,
                            'processing_date' => $date_now,
                            );
                        $updated = $this->Client_manage_model->update_status($array,$id);
                        }
                        
                    }

                    public function send_mail()
                    {
                        $post = $this->input->post();
                        $id = $post['id'];
                        $date_now = date('d/m/Y');
                        if( $post['status'] == 'incorrect'){
                            $array = array(
                            'status' => $post['status'],
                            'incorrect_date' => $date_now,
                            );
                        $updated = $this->Client_manage_model->update_status($array,$id);
                        return 1;
                        }
                    }



               
                       
}