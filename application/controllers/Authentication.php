<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller {
    
                public function index()
                     {
                      $this->load->view('login');
                      $this->load->library('session');
                     }

                    public function validate()
                            { 
                            if ($_POST) {     
                            
                            $this->load->library('form_validation');
                            $this->form_validation->set_rules('email','uname','required');
                            $this->form_validation->set_rules('password','pwd','required');
                            
                            if($this->form_validation->run() == TRUE)
                            {
                            $username=$this->input->post('email');
                            $password=$this->input->post('password');
                            
                            // $login_id=$this->Authenticationmodal->check($username,$password);
                            // if ($login_id) 
                            // {
                            //  //$this->session->sess_expiration='60';   
                            // $this->session->set_userdata('admin_id',$login_id);
                            redirect('serviceMaster'); 
                            // }else
                            // {
                            // $this->session->set_flashdata('login_failed','Invalid Username and Pasword');
                            // return redirect('Authentication');
                            // }
                            
                            }else{
                            $this->load->view('login');
                            }
                            
                            }
                    }
                    
                    public function HomePage()
                     {
                      $this->load->view('Finance/home');
                     }
     
                     
                      public function changePass()
                     {
                       $data['count']=$this->Report_model->get_count();
                        $count1=$data['count'][0]->count_rows;
                            $get_count=$this->Report_model->get_count_id();
                            $count2=$get_count[0]->count_row;
                            $total_count=$count1-$count2;
                            $all=$total_count*10;
                            $data['curr_stock']=$all;
                       $this->load->view('Finance/change_pass',$data);
                     }
     
                    public function update_password(){
                        $admin_id = $this->session->userdata('admin_id');
                        $id = $admin_id->finance_id;
                        $post=$this->input->post();
                        $old_pass=$post['password'];
                        $new_pass=	$post['confirm_pass'];
                        $array= array('finance_password'=>$new_pass);
                        $this->load->model('Authenticationmodal');
                        //$this->Authenticationmodal->docpassword($id,$old_pass);
                        if($post['confirm_pass']==$post['confirm_password']){
                        $update_pass = $this->Authenticationmodal->change_pass($id,$array);
                        if ($update_pass == TRUE)
                        {
                        echo  ("<script type='text/javascript'>alert('Password changed successfully');    	
                        
                        window.location.href='http://alphaobs.com/MIND/Finance/dashboard';
                        </script>");
                        
                        } else {
                        echo  ("<script type='text/javascript'>alert('Password is not change .Try again');    	
                        
                        // 			window.location.href='http://alphaobs.com/MIND/Finance/change_password';
                        </script>");
                        }		
                        }
                        $this->session->unset_userdata('admin_id');
                        return redirect('Authentication');
                        
                        }
                        
                        public function logout()
                        { 
                        $this->session->unset_userdata('admin_id');
                        redirect('welcome');
                        
                        }

}	
