<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Report extends CI_Controller{

	public function client_wise_report()
	{
		$this->load->model('Report_model');
	//	$data['posts'] = $this->Report_model->get_clients();
		$this->load->view('Admin/client_wise_report');
	}

	public function service_wise_report()
	{
		// $this->load->model('Report_model');
		// $data['posts'] = $this->Report_model->get_services();
		$this->load->view('Admin/service_wise_report');
	}

	public function get_client_wise_report()
	{
		// $this->load->model('Report_model');
		// $client=$this->input->post('client_name');
		// //print_r($client);exit();	
		// $from=$this->input->post('from');
		// $to=$this->input->post('to');		
		// $data['client_data'] = $this->Report_model->get_client_data($client,$from,$to);
		//print_r($data);exit();
		$this->load->view('Admin/get_client_wise_report');
	}

	public function get_service_wise_report()
	{
		// $this->load->model('Report_model');
		// $service=$this->input->post('service_name');		
		// $from=$this->input->post('from');
		// $to=$this->input->post('to');		
		// $data['client_data'] = $this->Report_model->get_service_data($service,$from,$to);
		//print_r($data);exit();
		$this->load->view('Admin/get_service_wise_report');
	}
}

?>