<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company extends CI_Controller {
    
    public function index()
        {
            $this->load->model('Agent_model');
           
        }

         public function show_company()
        {
            $data['company_data'] = $this->Company_manage_model->show_company_data(); 
           // print_r($data);exit();
            $this->load->view('Admin/show_company',$data);
           
        }
        public function add_company()
        {
            $this->load->view('Admin/add_company');
           
        }

        public function save_company()
           {   
           //print_r($_POST) ;exit();         
                                $post = $this->input->post();
                                $dir = json_encode($this->input->post('director')); 
                                $email = json_encode($this->input->post('email'));    
                              //  print_r($email);exit();                         
                               
                                $array = array(
                                'company_name' =>$post['company_name'],
                                'id_no' =>$post['identification_no'],
                                'director_name' =>$dir,
                                'email' =>$email,                                
                                'landline' =>$post['landline'],
                                //'frequency' =>$post['frequency'],
                                'address' =>$post['address'],  
                                'remark' =>$post['remark'],                                
                                );  
                               // print_r($array);exit();                            
                               $this->load->model('Company_manage_model');
                                $inserted = $this->Company_manage_model->save_company_data($array);                             
                                
                                if ($inserted == TRUE)
                                {                               
                                    $this->session->set_flashdata('msg', '<div class="alert alert-success">You have added Company Data  Successfully</div>');
                                    redirect('show_company');
                               
                                } else {
                                 $this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                                    redirect('show_company');
                                }
        
             } 

             public function edit_company_details()
            { 
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
                        $comp_id= $this->encrypt->decode($seg_id);                        
                        $data['company_data'] = $this->Company_manage_model->edit_company_data($comp_id);   
                       // print_r($data);exit();    
                        $this->load->view('Admin/edit_company',$data);
            }  

            public function dashboard()
            { 
                         
                        $this->load->view('Admin/dashboard');
            } 
             public function assign_to_staff()
            { 
                         
                $this->load->view('Admin/assign_to_staff');
            } 


    }