<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Service_management extends CI_Controller{

	public function show_service()
	{
		$this->load->model('Service_manage_model');
		//$data['posts'] = $this->Service_manage_model->get_services();
		$this->load->view('Admin/show_service');
	}

	public function add_service()
	{
		$this->load->view('Admin/add_service');
	}

	public function save_service()
	{
		//print_r($_POST);exit();
		$post = $this->input->post();
		$date=date('d/m/Y');
		
		$monthly_date = $this->input->post('monthly_date');
		$annualy_date = $this->input->post('annualy_date');
		$annualy_month = $this->input->post('annually_month');
		$annualy=$annualy_date.'/'.$annualy_month;
		$queterly_date1 = $this->input->post('quterly_date1');
		$queterly_month1 = $this->input->post('quterly_month1');
		$queterly_date2 = $this->input->post('quterly_date2');
		$queterly_month2 = $this->input->post('quterly_month2');
		$queterly_date3 = $this->input->post('quterly_date3');
		$queterly_month3 = $this->input->post('quterly_month3');
		$queterly_date4 = $this->input->post('quterly_date4');
		$queterly_month4 = $this->input->post('quterly_month4');

		$queterly1=$queterly_date1.'/'.$queterly_month1;
		$queterly2=$queterly_date2.'/'.$queterly_month2;
		$queterly3=$queterly_date3.'/'.$queterly_month3;
		$queterly4=$queterly_date4.'/'.$queterly_month4;
		//print_r($queterly1);print_r($queterly2);print_r($queterly3);print_r($queterly4);exit();

		$this->load->model('Service_manage_model');
		$array = array(
               'service_name' => $post['service_name'],
               'frequency' => $post['frequency'],
              // 'due_date' => $post['due_date'],
               'monthly_date' => $monthly_date,
               'queterly_date1' => $queterly1,
               'queterly_date2' => $queterly2,
               'queterly_date3' => $queterly3,
               'queterly_date4' => $queterly4,
               'annually_date' => $annualy,
               'date_created'=>$date
		);
		$inserted = $this->Service_manage_model->store_service($array);                              
		$this->session->set_flashdata('msg', '<div class="alert alert-success">You have added Service Successfully</div>');
		redirect('serviceMaster');

	}

	public function edit_service()
	{
		$this->load->model('Service_manage_model');
		$seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	    $service_id = $this->encrypt->decode($seg_id);
		$data['posts'] = $this->Service_manage_model->get_one_service($service_id);
		//print_r($data);exit();
		$this->load->view('Admin/edit_service',$data);
	}

	public function update_service()
	{
		$post = $this->input->post();

		$date=date('d/m/Y');
		
		$monthly_date = $this->input->post('monthly_date');
		$annualy_date = $this->input->post('annualy_date');
		$annualy_month = $this->input->post('annually_month');
		$annualy=$annualy_date.'/'.$annualy_month;
		$queterly_date1 = $this->input->post('quterly_date1');
		$queterly_month1 = $this->input->post('quterly_month1');
		$queterly_date2 = $this->input->post('quterly_date2');
		$queterly_month2 = $this->input->post('quterly_month2');
		$queterly_date3 = $this->input->post('quterly_date3');
		$queterly_month3 = $this->input->post('quterly_month3');
		$queterly_date4 = $this->input->post('quterly_date4');
		$queterly_month4 = $this->input->post('quterly_month4');

		$queterly1=$queterly_date1.'/'.$queterly_month1;
		$queterly2=$queterly_date2.'/'.$queterly_month2;
		$queterly3=$queterly_date3.'/'.$queterly_month3;
		$queterly4=$queterly_date4.'/'.$queterly_month4;
		$sv_id = $post['sv_id'];
		$array = array(
               'service_name' => $post['service_name'],
               'frequency' => $post['frequency'],
               //'due_date' => $post['due_date']
               'monthly_date' => $monthly_date,
               'queterly_date1' => $queterly1,
               'queterly_date2' => $queterly2,
               'queterly_date3' => $queterly3,
               'queterly_date4' => $queterly4,
               'annually_date' => $annualy,
               'date_created'=>$date
		);
		$this->load->model('Service_manage_model');
        $update = $this->Service_manage_model->update_service($sv_id,$array);
        $this->session->set_flashdata('msg', '<div class="alert alert-success">You have Updated Service Data Successfully</div>');
            redirect('serviceMaster');
        
	}

	public function delete_service_details()
	{
		$this->load->model('Service_manage_model');
		$seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	    $service_id = $this->encrypt->decode($seg_id);
	    $deleted = $this->Service_manage_model->delete_service_details($service_id);
	    $this->session->set_flashdata('msg', '<div class="alert alert-success">You have Deleted Data Successfully</div>');
        redirect('serviceMaster');
	}

	public function add_stages()
	{
		$this->load->view('Admin/add_stage');
	}

	public function add_checklist()
	{
		$this->load->view('Admin/add_checklist');
	}




}

?>