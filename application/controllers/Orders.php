<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orders extends MY_Controller {
    
    public function search_data(){
        $data['curr_stock'] = $this->eseal_stock;
        $order_no=$this->input->post('order_no');
        $this->load->model('Order_model');
        $data['count']=$this->Order_model->get_count_by_exp($order_no);
        $total=$data['count']->total;
        $this->load->library('pagination');
        $config['base_url'] = base_url().'search_order/';
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 5;
        $config['full_tag_open'] = "<ul class='pagination pull-center'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = "<li class='paginate_button '>";
        $config['num_tag_close'] = "</li>";
        $config['cur_tag_open'] = "<li class='active'><a aria-controls='example' data-dt-idx='3' tabindex='0'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li class='paginate_button '>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='paginate_button '>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li class='paginate_button '>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li class='paginate_button '>";
        $config['last_tagl_close'] = "</li>";
        $config['first_link'] = 'FIRST';
        $config['prev_link'] = 'PREVIOUS';
        $config['next_link'] = 'NEXT';
        $config['last_link'] = 'LAST';
        $this->pagination->initialize($config);
        $prod_id = $this->uri->segment(3);
        $data['prod_id']=$prod_id;
    	if(empty($prod_id))
    	{        $product_id = 0;
    	}else{
            $product_id = $prod_id;
    	}
    	$data['links']=$this->pagination->create_links();
        $data['orders']=$this->Order_model->search_data($order_no,$product_id,$config['per_page']);
        $this->load->view('Finance/search_order',$data);
    }
    
    public function get_orders()
        {
        $data['curr_stock'] = $this->eseal_stock;
        $data['count_eseal']=$this->Order_model->get_count();
        $total=$data['count_eseal']->total;      
        $this->load->library('pagination');
        $config['base_url'] = base_url().'show_order/';
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 5;
            $config['full_tag_open'] = "<ul class='pagination pull-center'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = "<li class='paginate_button '>";
            $config['num_tag_close'] = "</li>";
            $config['cur_tag_open'] = "<li class='active'><a aria-controls='example' data-dt-idx='3' tabindex='0'>";
            $config['cur_tag_close'] = "</a></li>";
            $config['next_tag_open'] = "<li class='paginate_button '>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li class='paginate_button '>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li class='paginate_button '>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li class='paginate_button '>";
            $config['last_tagl_close'] = "</li>";
            $config['first_link'] = 'FIRST';
            $config['prev_link'] = 'PREVIOUS';
            $config['next_link'] = 'NEXT';
            $config['last_link'] = 'LAST';
        $this->pagination->initialize($config);
        $prod_id = $this->uri->segment(2);
        $data['prod_id']=$prod_id;
    	if(empty($prod_id))
    	{        $product_id = 0;
    	}else{
            $product_id = $prod_id;
    	}
    	$data['links']=$this->pagination->create_links();
        $data['orders']=$this->Order_model->get_orders($product_id,$config['per_page']);
        $this->load->view('Finance/show_orders',$data);
        } 
        
    public function search_confirm_order(){
        $data['curr_stock'] = $this->eseal_stock;
        $order_no=$this->input->post('order_no');
         $this->load->model('Order_model');
        $data['count']=$this->Order_model->get_confirm_order_by_exp($order_no);
        $total=$data['count']->total;
        $this->load->library('pagination');
        $config['base_url'] = base_url().'search_confirmed_order/';
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 5;
        $config['full_tag_open'] = "<ul class='pagination pull-center'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = "<li class='paginate_button '>";
        $config['num_tag_close'] = "</li>";
        $config['cur_tag_open'] = "<li class='active'><a aria-controls='example' data-dt-idx='3' tabindex='0'>";
        $config['cur_tag_close'] = "</a></li>";
        $config['next_tag_open'] = "<li class='paginate_button '>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li class='paginate_button '>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li class='paginate_button '>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li class='paginate_button '>";
        $config['last_tagl_close'] = "</li>";
        $config['first_link'] = 'FIRST';
        $config['prev_link'] = 'PREVIOUS';
        $config['next_link'] = 'NEXT';
        $config['last_link'] = 'LAST';
        $this->pagination->initialize($config);
        $prod_id = $this->uri->segment(2);
        $data['prod_id']=$prod_id;
    	if(empty($prod_id))
    	{        $product_id = 0;
    	}else{
            $product_id = $prod_id;
    	}
    	$data['links']=$this->pagination->create_links();
        $data['orders']=$this->Order_model->search_confirmed_orders($order_no,$product_id,$config['per_page']);
        $this->load->view('Finance/search_confirmed_order',$data);
    }
        
    public function get_confirmed_orders()
        {
        $data['curr_stock'] = $this->eseal_stock;
        $data['count_eseal']=$this->Order_model->confirmed_order_count();
        $total=$data['count_eseal']->total;      
        $this->load->library('pagination');
        $config['base_url'] = base_url().'show_confirmed_order/';
        $config['total_rows'] = $total;
        $config['per_page'] = 10;
        $config['use_page_numbers'] = TRUE;
        $config['num_links'] = 5;
            $config['full_tag_open'] = "<ul class='pagination pull-center'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = "<li class='paginate_button '>";
            $config['num_tag_close'] = "</li>";
            $config['cur_tag_open'] = "<li class='active'><a aria-controls='example' data-dt-idx='3' tabindex='0'>";
            $config['cur_tag_close'] = "</a></li>";
            $config['next_tag_open'] = "<li class='paginate_button '>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li class='paginate_button '>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li class='paginate_button '>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li class='paginate_button '>";
            $config['last_tagl_close'] = "</li>";
            $config['first_link'] = 'FIRST';
            $config['prev_link'] = 'PREVIOUS';
            $config['next_link'] = 'NEXT';
            $config['last_link'] = 'LAST';
        $this->pagination->initialize($config);
        $prod_id = $this->uri->segment(2);
        $data['prod_id']=$prod_id;
    	if(empty($prod_id))
    	{        $product_id = 0;
    	}else{
            $product_id = $prod_id;
    	}
    	$data['links']=$this->pagination->create_links();
        $data['orders']=$this->Order_model->get_confirmed_orders($product_id,$config['per_page']);
        $orginal_qty=$data['orders'][0]->ORDER_QTY;
        $this->load->view('Finance/show_confirmed_orders',$data);
        } 
        
        
         public function view_payment_details()
        { 
            $id = $this->input->post("id");
             $prod_id = $this->input->post("prod_id");
            $data['posts'] = $this->Order_model->payment_proof($id);
            foreach($data as $key => $value){
            foreach($value as $key1 => $value1){
                echo "<input type='hidden' name='prod_id' value=$prod_id>";
            echo "<input type='hidden' name='order_id' value='".$value1->O_ID."'>";
            echo" <div class='form-group'><label for='Exporter name' class='form-control-label'><b>Exporter Name</b></label><input type='text' name='exp_name' class='form-control' value='".$value1->exporter_name."' readonly=true></div>
            <div class='form-group'><label for='Payment Proof' class='form-control-label'><b>Payment Proof</b></label><textarea type='text' rows='3'  name='ordered_qty' class='form-control' readonly=true>$value1->PAYMENT_PROOF</textarea></div>";
            if($value1->QTY_TO_ALLOCATE_THIS_TIME=='0'){  
             echo"<div class='form-group'><label for='Ordered Qty' class='form-control-label'><b>Order Qty. </b></label><input type='text' name='ordered_qty' id='kk' class='form-control' value='".$value1->ORDER_QTY."' readonly=true ></div>";
            } else {
            echo"<div class='form-group'><label for='Ordered Qty' class='form-control-label'><b>Order Qty. </b></label><input type='text' name='ordered_qty' id='kk' class='form-control' value='".$value1->QTY_TO_ALLOCATE_THIS_TIME."' readonly=true ></div>";
           }
           if($value1->ORDER_STATUS == 'send_to_dispatch' && $value1->PAYMENT_RECEIVED_STATUS == 'Payment_received') {
            echo" <div class='form-group'> 
            <label class='checkbox-inline form-control-label'>
            <input type='checkbox' name='payment_rcv_status' value='Payment_received' checked>&nbsp;<b>Mark Payment as Received</b> 
            </label></div>
            <div class='form-group'> <label class='checkbox-inline form-control-label'>
            <input type='checkbox'  name='send_to_dispatch' value='send_to_dispatch' checked>&nbsp;<b>Send to Dispatch</b> 
            </label></div>"; }
             elseif ($value1->PAYMENT_RECEIVED_STATUS == 'Payment_received'){ 
                  echo" <div class='form-group'> 
            <label class='checkbox-inline form-control-label'>
            <input type='checkbox' name='payment_rcv_status' value='Payment_received' checked>&nbsp;<b>Mark Payment as Received</b> 
            </label></div>
            <div class='form-group'> <label class='checkbox-inline form-control-label'>
            <input type='checkbox'  name='send_to_dispatch' value='send_to_dispatch' >&nbsp;<b>Send to Dispatch</b> 
            </label></div>"; }
            elseif ($value1->ORDER_STATUS == 'send_to_dispatch'){
                  echo" <div class='form-group'> 
            <label class='checkbox-inline form-control-label'>
            <input type='checkbox' name='payment_rcv_status' value='Payment_received' >&nbsp;<b>Mark Payment as Received</b> 
            </label></div>
            <div class='form-group'> <label class='checkbox-inline form-control-label'>
            <input type='checkbox'  name='send_to_dispatch' value='send_to_dispatch' checked>&nbsp;<b>Send to Dispatch</b> 
            </label></div>"; } else {
                echo" <div class='form-group'> 
            <label class='checkbox-inline form-control-label'>
            <input type='checkbox' name='payment_rcv_status' value='Payment_received' >&nbsp;<b>Mark Payment as Received</b> 
            </label></div>
            <div class='form-group'> <label class='checkbox-inline form-control-label'>
            <input type='checkbox'  name='send_to_dispatch' value='send_to_dispatch' >&nbsp;<b>Send to Dispatch</b> 
            </label></div>";
            }
            echo" <div class='form-group'><label for='date1' class='form-control-label'><b>Date Payment was Received</b></label><input type='text' placeholder='Pick the date'  id='payment_date'  name='payment_recv_date' required  class='form-control datepicker' value='".$value1->PAYMENT_RECEIVED_DATE."' ></div>";
             
            }
            }
        }
        
        	public function payment_status_update()
						{
					if($_POST){
					
					    $order_id = $this->input->post('order_id');
					    $prod_id = $this->input->post('prod_id');
					    $quantity=$this->Order_model->get_order_qty($order_id);
                       	$post = $this->input->post();
                       	$date= DATE('d/m/Y');
                       	$admin_id = $this->session->userdata('admin_id');
                        $user_id = $admin_id->finance_id;
						$array = array(
						'PAYMENT_RECEIVED_STATUS' =>$post['payment_rcv_status'],
						'ORDER_STATUS' =>$post['send_to_dispatch'],
						'PAYMENT_RECEIVED_DATE' =>$post['payment_recv_date'],
						);
					
						$updated =$this->Order_model->update_payment_status($array,$order_id);
						if ($updated==TRUE)
						{
                        $this->session->set_flashdata('msg', '<div class="alert alert-success">Order has been sent to Dispatch.</div>');
                         if($prod_id==0){
                            redirect('show_order');
                            }else{
                               redirect('show_order/'.$prod_id);
						    }
                        
						}else {
						$this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                         if($prod_id==0){
                            redirect('show_order');
                            }else{
                               redirect('show_order/'.$prod_id);
						    }
						}
						}
					    }
					    
                    public function view_order_details()
                        { 
                        $id = $this->input->post("id");
                         $prod_id = $this->input->post("prod_id");
                        $data['posts'] = $this->Order_model->viewOrder($id);
                        $amt=$data['posts'][0]->AMOUNT;
                        $orginal_qty=$data['posts'][0]->ORDER_QTY;
                        $commission=$data['posts'][0]->COMMISSION;
                        //print_r($total1);
                        $get_sum_qty =$this->Order_model->get_qty($id);
                        $get_sum=$get_sum_qty[0]->sum;
                        $pending_qty= $orginal_qty-$get_sum;
                        foreach($data as $key => $value){
                        foreach($value as $key1 => $value1){
                        echo "<input type='hidden' name='prod_id' value=$prod_id>";
                        echo "<input type='hidden' name='order_id' id='unique_id' value='".$value1->O_ID."'>";
                        echo" <div class='form-group'><label for='Exporter Name' class='form-control-label'><b>Exporter Name</b></label><input type='text' name='exp_name' class='form-control' value='".$value1->exporter_name."' readonly=true></div> 
                        <div class='form-group'><label for='Order Quantity' class='form-control-label'><b>Quantity to be Ordered</b></label><input type='text'  name='ordered_qty' id='ordered_qtys' class='form-control' value='".$value1->ORDER_QTY."' readonly=true></div>";
                        echo "<div class='form-group'><label for='Qty. for proforma' class='form-control-label'><b>Quantity for which Proforma has to be Generated</b></label><input type='text'   name='allocated_qty' id='allocated_qty' class='form-control'  value='".$value1->ORDER_QTY."'></div>";
                        echo" <div class='form-group'><label for='Amount' class='form-control-label'><b>Amount</b></label><input type='text' name='rate' id='rates'  class='form-control' value='".$value1->AMOUNT."'></div>
                        <div class='form-group'><label for='Commission' class='form-control-label'><b>Commission</b></label><input type='text' name='commission' id='commissions' class='form-control' value='".$value1->COMMISSION."'>
                        </div>";
                           echo" <div class='modal-footer d-flex justify-content-center'>
                         <a class='btn btn-success' id='demo1' type='submit' ><font color='white'>Generate Proforma </font></a>
                         </div>"; 
                          echo "<script>
                 $('#demo1').click(function(){
                 var oreder_qty=document.getElementById('ordered_qtys').value;
                 var rates=document.getElementById('rates').value;
                 var commissions=document.getElementById('commissions').value;
                 var total1= Number(rates) + Number(commissions);
                 if(oreder_qty>0){
                     if(oreder_qty<=200){
                         if(total1>=275){
                             $('#myform5').submit();
                         }else{
                             alert('Total of E-seal rate and commission has to be minimum 275 for this order quantity.');
                         }
                     }
                     if(oreder_qty>200 && oreder_qty<=500){
                         if(total1>=255){
                             $('#myform5').submit();
                         }else{
                             alert('Total of E-seal rate and commission has to be minimum 255 for this order quantity.');
                         }
                     }
                     if(oreder_qty>500 && oreder_qty<=1000){
                         if(total1>=225){
                             $('#myform5').submit();
                         }else{
                             alert('Total of E-seal rate and commission has to be minimum 225 for this order quantity.');
                         }
                     }
                     if(oreder_qty>1000){
                         if(total1>=200){
                             $('#myform5').submit();
                         }else{
                             alert('Total of E-seal rate and commission has to be minimum 200 for this order quantity.');
                         }
                     }
                 }
            
                });
                </script>";
    
                        
                        }
                        }
                        }
                        
        
        	public function generate_proforma_update()
						{
					if($_POST){
					   // print_r($_POST);exit();
					    $prod_id = $this->input->post('prod_id');
					    $order_id = $this->input->post('order_id');
						$order_qty = $this->input->post('ordered_qty');
						$allocated_qty = $this->input->post('allocated_qty');
					    $rate = $this->input->post('rate');
					    $commission = $this->input->post('commission');
					    $total1=$rate+$commission;
					        if(!empty($allocated_qty)){
					        $total_amt=$allocated_qty*$rate;
					        $total_cmsn=$allocated_qty*$commission;
					    } else {
					        $total_amt=$order_qty*$rate;
					       $total_cmsn=$order_qty*$commission;
					    } 
					    $quantity=$this->Order_model->get_order_qty($order_id);
					    $original_qty =$quantity[0]->ORDER_QTY; 
					    if($original_qty < $allocated_qty){
					        $this->session->set_flashdata('error_flash','Quantity to be allocated is greater than ordered Quantity.');
					        redirect('show_order');
					    } else{
                       	$post = $this->input->post();
                       	$date= DATE('d/m/Y');
                       	$admin_id = $this->session->userdata('admin_id');
                        $user_id = $admin_id->finance_id;
                       	$post = $this->input->post();
                       	
						$order_array = array(
						'ORDER_QTY' =>$order_qty,
						'AMOUNT' =>$rate,
						'TOTAL_AMOUNT' =>$total_amt,
						'COMMISSION' =>$commission,
						'TOTAL_COMMISSION' =>$total_cmsn,
						'QTY_TO_ALLOCATE_THIS_TIME' =>$post['allocated_qty'],
						'ORDER_STATUS' =>'Proforma_generated'
						);
						
						$array1 = array(
						'O_ID' =>$order_id,
						'QUANTITY' =>$post['allocated_qty'],
						'USER_ID' =>$user_id,
						'DATE' =>$date
						);
						$inserted =$this->Order_model->insert_payment_status($array1);
						$updated =$this->Order_model->update_order($order_array,$order_id);
						
						if ($updated==TRUE)
						{
						    $this->session->set_flashdata('msg', '<div class="alert alert-success">Proforma Generated Successfully</div>');
                            $data['posts'] = $this->Order_model->get_pdf_data($order_id);
                            //print_r($data['posts']);exit();
                                  foreach($data['posts'] as $row){
                                    $cgst=9/100*$row->TOTAL_AMOUNT;
                                    $tax=$cgst+$cgst;
                                    $igst=18/100*$row->TOTAL_AMOUNT;
                                    $number=$row->TOTAL_AMOUNT+$cgst+$cgst;
                                     $data['gst']=$row->gst_no[0].$row->gst_no[1];
                                    $no = round($number);                   // Covert chargable amount
                                    $point = round($number - $no, 2) * 100;
                                    $hundred = null;
                                    $digits_1 = strlen($no);
                                    $i = 0;
                                    $str = array();
                                    $words = array('0' => '', '1' => 'One', '2' => 'Two',
                                    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                                    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                                    '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                                    '13' => 'Thirteen', '14' => 'Fourteen',
                                    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                                    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                                    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                                    '60' => 'Sixty', '70' => 'Seventy',
                                    '80' => 'Eighty', '90' => 'Ninety');
                                    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
                                    while ($i < $digits_1) {
                                    $divider = ($i == 2) ? 10 : 100;
                                    $number = floor($no % $divider);
                                    $no = floor($no / $divider);
                                    $i += ($divider == 10) ? 1 : 2;
                                    if ($number) {
                                    $plural = (($counter = count($str)) && $number > 9) ? 's' : null;
                                    $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                                    $str [] = ($number < 21) ? $words[$number] .
                                    " " . $digits[$counter] . $plural. " " 
                                    :
                                    $words[floor($number / 10) * 10]
                                    . " " . $words[$number % 10] . " "
                                    . $digits[$counter] . $plural." ";
                                    } else $str[] = null;
                                    }
                                    $str = array_reverse($str);
                                    $result = implode('', $str);
                                    $points = ($point) ?
                                    "." . $words[$point / 10] . " " . 
                                    $words[$point = $point % 10] : '';
                                     $data['total_amt']=$result;  // Chargable amt end
                                    
                                    $no = round($tax);         // Convert Tax amount Start
                                    $point = round($tax - $no, 2) * 100;
                                    $hundred = null;
                                    $digits_1 = strlen($no);
                                    $i = 0;
                                    $str = array();
                                    $words = array('0' => '', '1' => 'One', '2' => 'Two',
                                    '3' => 'Three', '4' => 'Four', '5' => 'Five', '6' => 'Six',
                                    '7' => 'Seven', '8' => 'Eight', '9' => 'Nine',
                                    '10' => 'Ten', '11' => 'Eleven', '12' => 'Twelve',
                                    '13' => 'Thirteen', '14' => 'Fourteen',
                                    '15' => 'Fifteen', '16' => 'Sixteen', '17' => 'Seventeen',
                                    '18' => 'Eighteen', '19' =>'Nineteen', '20' => 'Twenty',
                                    '30' => 'Thirty', '40' => 'Forty', '50' => 'Fifty',
                                    '60' => 'Sixty', '70' => 'Seventy',
                                    '80' => 'Eighty', '90' => 'Ninety');
                                    $digits = array('', 'Hundred', 'Thousand', 'Lakh', 'Crore');
                                    while ($i < $digits_1) {
                                    $divider = ($i == 2) ? 10 : 100;
                                    $tax = floor($no % $divider);
                                    $no = floor($no / $divider);
                                    $i += ($divider == 10) ? 1 : 2;
                                    if ($tax) {
                                    $plural = (($counter = count($str)) && $tax > 9) ? 's' : null;
                                    $hundred = ($counter == 1 && $str[0]) ? ' and ' : null;
                                    $str [] = ($tax < 21) ? $words[$tax] .
                                    " " . $digits[$counter] . $plural. " " 
                                    :
                                    $words[floor($tax / 10) * 10]
                                    . " " . $words[$tax % 10] . " "
                                    . $digits[$counter] . $plural." ";
                                    } else $str[] = null;
                                    }
                                    $str = array_reverse($str);
                                    $result = implode('', $str);
                                    $points = ($point) ?
                                    "." . $words[$point / 10] . " " . 
                                    $words[$point = $point % 10] : '';
                                     $data['tax_amt']=$result;
                                    
                                    // $con_no=$this->convert_number($number);
                                    // $total_tax=$this->convert_number($tax);
                                    // $data['word_no']=$con_no;
                                    // $data['tx']=$total_tax;
                            }
                            $exp_email=$data['posts'][0]->exp_email;
                            $agent_email=$data['posts'][0]->agent_email;
                            $this->load->view('Finance/pdf_page',$data); //Load  pdf page
                            $html = $this->output->get_output(); // Pdf generate code
                    		$this->load->library('pdf');
                    		$this->dompdf->loadHtml($html);
                    		$this->dompdf->setPaper('A4','portrait');
                    		$this->dompdf->render();
                    		$this->dompdf->stream('welcome.pdf', array('Attachment'=>0)); 
                    		$output = $this->dompdf->output(); // end pdf code
                    		if($data['posts'][0]->ORDER_THROUGH=="Exporter"){
                    		    // Email to exporter
                                $config = Array(
                                'protocol'  => 'smtp',
                                'smtp_host' => 'ssl://bh-in-28.webhostbox.net',
                                'smtp_port' => '465',
                                'smtp_user' => 'mindeseals@alphaobs.com',
                                'smtp_pass' => '?X{Z=7(Ha-Bx',
                                'mailtype'  => 'html',
                                'starttls'  => true,
                                'newline'   => "\r\n"
                                );
                                
                                $this->load->library('email',$config);
                                $this->email->from('mindeseals@alphaobs.com');
                                $this->email->to($exp_email);
                                $this->email->cc('');
                                $this->email->bcc('');
                                $this->email->subject('PDF');
                                $this->email->message();
                                $this->email->attach($output, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);
                                if ( ! $this->email->send())
                                {
                                echo $this->email->print_debugger();
                                }  
                                }
                                else {
                                    // Mail to Agent
                                $config = Array( 
                                'protocol'  => 'smtp',
                                'smtp_host' => 'ssl://bh-in-28.webhostbox.net',
                                'smtp_port' => '465',
                                'smtp_user' => 'mindeseals@alphaobs.com',
                                'smtp_pass' => '?X{Z=7(Ha-Bx',
                                'mailtype'  => 'html',
                                'starttls'  => true,
                                'newline'   => "\r\n"
                                );
                                $this->load->library('email',$config);
                                $this->email->from('mindeseals@alphaobs.com');
                                $this->email->to($exp_email);
                                $this->email->cc('');
                                $this->email->bcc('');
                                $this->email->subject('PDF');
                                $this->email->message();
                                $this->email->attach($output, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);
                                if ( ! $this->email->send())
                                {
                                echo $this->email->print_debugger();
                                }
                                 // Email to Agent
                                 
                                $config = Array(
                                'protocol'  => 'smtp',
                                'smtp_host' => 'ssl://bh-in-28.webhostbox.net',
                                'smtp_port' => '465',
                                'smtp_user' => 'mindeseals@alphaobs.com',
                                'smtp_pass' => '?X{Z=7(Ha-Bx',
                                'mailtype'  => 'html',
                                'starttls'  => true,
                                'newline'   => "\r\n"
                                );
                                $this->load->library('email',$config);
                                $this->email->from('mindeseals@alphaobs.com');
                                $this->email->to($agent_email);
                                $this->email->cc('');
                                $this->email->bcc('');
                                $this->email->subject('PDF');
                                $this->email->message();
                                $this->email->attach($output, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);
                                if ( ! $this->email->send())
                                {
                                echo $this->email->print_debugger();
                                }
                                
                              }
						    } 
						    }
                          
                         
					    }
                           
         } 
                        
                    public function view_details()
                        {
                        $data['curr_stock'] = $this->eseal_stock;
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $seg_id= $this->encrypt->decode($seg_id);
                        $data['order_detail'] = $this->Order_model->view_all_details($seg_id);
                        $this->load->view('Finance/view_order_details',$data);
                        }
                        
                    public function view_courier_details()
                        {
                        $prod_id = $this->input->post('prod_id');
                        $id = $this->input->post("id");
                         $prod_id = $this->input->post("prod_id");
                        $data['posts'] = $this->Order_model->courier_detail($id);
                        foreach($data as $key => $value){
                        foreach($value as $key1 => $value1){
                        echo "<input type='hidden' name='prod_id' value=$prod_id>";
                        echo "<input type='hidden' name='order_id' value='".$value1->O_ID."'>";
                        if (!empty($value1->COURIER_DETAIL)){
                        echo" <div class='form-group'><label for='Courier detail' class='form-control-label'><b>Courier detail</b></label><textarea type='text' rows='3' name='courier_deatil' class='form-control' disabled>$value1->COURIER_DETAIL</textarea></div>"; 
                        }
                         else {
                           echo" <div class='form-group'><label for='Courier detail' class='form-control-label'><b>Courier detail</b></label><textarea type='text' rows='3' name='courier_deatil' class='form-control' >No Tracking Information Available </textarea></div>";   
                         }
                         }
                        }
                        }
                    
                   
                        
                          function convert_number($number) {
                            if (($number < 0) || ($number > 999999999)) {
                            throw new Exception("Number is out of range");
                            }
                            $Gn = floor($number / 1000000);
                            /* Millions (giga) */
                            $number -= $Gn * 1000000;
                            $kn = floor($number / 1000);
                            /* Thousands (kilo) */
                            $number -= $kn * 1000;
                            $Hn = floor($number / 100);
                            /* Hundreds (hecto) */
                            $number -= $Hn * 100;
                            $Dn = floor($number / 10);
                            /* Tens (deca) */
                            $n = $number % 10;
                            /* Ones */
                            $res = "";
                            if ($Gn) {
                            $res .= $this->convert_number($Gn) .  "Million";
                            }
                            if ($kn) {
                            $res .= (empty($res) ? "" : " ") .$this->convert_number($kn) . " Thousand";
                            }
                            if ($Hn) {
                            $res .= (empty($res) ? "" : " ") .$this->convert_number($Hn) . " Hundred";
                            }
                            $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", "Nineteen");
                            $tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy", "Eigthy", "Ninety");
                            if ($Dn || $n) {
                            if (!empty($res)) {
                            $res .= " and ";
                            }
                            if ($Dn < 2) {
                            $res .= $ones[$Dn * 10 + $n];
                            } else {
                            $res .= $tens[$Dn];
                            if ($n) {
                            $res .= "-" . $ones[$n];
                            }
                            }
                            }
                            if (empty($res)) {
                            $res = "zero";
                            }
                            return $res;
                            }
                        
         public function view_linking_details()
                        {
                        $data['curr_stock'] = $this->eseal_stock;
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $seg_id= $this->encrypt->decode($seg_id);
                        $this->load->model('Order_model');
                        $data['linking_detail'] = $this->Order_model->view_link_details($seg_id);
                        $this->load->view('Finance/view_linking_details',$data);
                        }
   
    
    
}