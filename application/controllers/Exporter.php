<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Exporter extends MY_Controller {
    
                            public function index()
                                {
                                $this->load->library('upload');
                                }
                                
                        public function exporter_list()
                            {
                            $data['curr_stock'] = $this->eseal_stock;
                            $this->load->model('Exporter_model');
                            $data['hosts']=$this->Exporter_model->exporter_data();
                            $this->load->view('Finance/exporter_list',$data);
                            } 
                                
                            public function add_exporters()
                                {
                                $data['curr_stock'] = $this->eseal_stock;
                                $this->load->model('Exporter_model');
                                $data['states']=$this->Exporter_model->get_state();
                                $data['hosts']=$this->Exporter_model->get_agent();
                                $this->load->view('Finance/add_exporter',$data);
                                } 
                                
                            function PwdHash($pwd, $salt = null)
                                {
                                if ($salt === null)     {
                                $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
                                }
                                else     {
                                $salt = substr($salt, 0, SALT_LENGTH);
                                }
                                return $salt . sha1($pwd . $salt);
                                }
                                
                            public function save_exporter_data()
                                {
                                if($_POST){
                                $doc_1 = "upload/gst_certificate/";
                                if(isset($_FILES['userfile_1']) && !empty($_FILES['userfile_1'])){
                                $filename = pathinfo($_FILES['userfile_1']['name']);
                                if(move_uploaded_file($_FILES['userfile_1']['tmp_name'],$doc_1.$filename['basename'])){
                                $attch_1 =  base_url().$doc_1.$filename['basename'];
                                }
                                else{
                                $attch_1 = "na";
                                }
                                }  
                                $doc_2 = "upload/iec_certificate/";
                                if(isset($_FILES['userfile_2']) && !empty($_FILES['userfile_2'])){
                                $filename = pathinfo($_FILES['userfile_2']['name']);
                                if(move_uploaded_file($_FILES['userfile_2']['tmp_name'],$doc_2.$filename['basename'])){
                                $attch_2 =  base_url().$doc_2.$filename['basename'];
                                }
                                else{
                                $attch_2 = "na";
                                }
                                }  
                                $doc_3 = "upload/pan_certificate/";
                                if(isset($_FILES['userfile_3']) && !empty($_FILES['userfile_3'])){
                                $filename = pathinfo($_FILES['userfile_3']['name']);
                                if(move_uploaded_file($_FILES['userfile_3']['tmp_name'],$doc_3.$filename['basename'])){
                                $attch_3 =  base_url().$doc_3.$filename['basename'];
                                }
                                else{
                                $attch_3 = "na";
                                }
                                } 
                                $post = $this->input->post();
                                $rand = substr(md5(microtime()),rand(0,26),6);
                                $sha1pass = $this->PwdHash($rand);
                                $temp_pass="password";
                                $email_add = $this->input->post('exp_email');
                                $array = array(
                                'agent_id' =>$post['agent_name'],
                                'exporter_name' =>$post['exp_name'],
                                'exp_email' =>$post['exp_email'],
                                'exp_mobile' =>$post['exp_mobile'],
                                'exp_password' =>$temp_pass,
                                'gst_no' =>$post['gst_no'],
                                'cst_no' =>$post['cst_no'],
                                'pan_no' =>$post['pan_no'],
                                'org_name' =>$post['organization_name'],
                                'bill_address' =>$post['billing_address'],
                                'billing_address_pincode' =>$post['billing_pincode'],
                                'shipped_address' =>$post['shipping_address'],
                                'shipping_address_pincode' =>$post['shipping_pincode'],
                                //'location' =>$post['location_name'],
                                'office_contact' =>$post['office_contact_no'],
                                'iec' =>$post['Iec_no'],
                                //'aadhar_no' =>$post['aadhar_no'],
                                'state_name' =>$post['state_name'],
                                'city_name' =>$post['city_name'],
                                //'pincode' =>$post['pin_code'],
                                'gst_cert' =>$attch_1,
                                'iec_cert' =>$attch_2,
                                'pan_copy' =>$attch_3
                                );
                               
                                $this->load->model('Exporter_model');
                                $check = $this->Exporter_model->check_email($email_add);
                                 $count_emailid=count($check);
                                // print_r($count_emailid);exit();
                                 if($count_emailid==0)
                                 {
                                $inserted = $this->Exporter_model->insert_exporter_data($array);
                                $em=$this->Exporter_model->getEmail($inserted);
                                $email_id=$em[0]->exp_email;
                                $iec=$em[0]->iec;
                                $email_fn = $this->sendEmail($email_id,$rand,$iec);
                                if ($inserted == TRUE)
                                {
                                    $this->session->set_flashdata('msg', '<div class="alert alert-success">Exporter Registered successfully</div>');
                                    redirect('add_exporter');
                               
                                } else {
                                 $this->session->set_flashdata('msg', '<div class="alert alert-success">Something went wrong, Please try again!</div>');
                                    redirect('add_exporter');
                                }
                               
                                 }
                                  else{
                                    echo  ("<script type='text/javascript'>alert('Email ID already exists , Please use another Email ID.');        	
                                 history.go(-1);
                               
                                </script>");
                                }
                                }
                                }
                                
                            public function sendEmail($email_id,$rand,$iec)
                                {
                                $config = Array(
                                'protocol'  => 'smtp',
                                'smtp_host' => 'ssl://bh-in-28.webhostbox.net',
                                'smtp_port' => '465',
                                'smtp_user' => 'mindeseals@alphaobs.com',
                                'smtp_pass' => '?X{Z=7(Ha-Bx',
                                'mailtype'  => 'html',
                                'starttls'  => true,
                                'newline'   => "\r\n"
                                );
                                
                                $this->load->library('email',$config);
                                $this->email->from('mindeseals@alphaobs.com');
                                $this->email->to($email_id);
                                $this->email->cc('');
                                $this->email->bcc('');
                                
                                $this->email->subject('Exporter Registration Successful – MIND E-Seals');
                                $data1 = "Dear Customer,<br> Welcome to MIND E-Seals! We are glad to have you on board with us and we will do our best 
                                to make sure you have a hassle free experience every time you use our product.<br>
                                This mail is a proof that your registration as a Exporter with us has been successfully 
                                completed and you are all set to get started.<br>Before you login for the first time please read
                                the “<b> NOTES : </b>“ section written in this Email.<br> Visit www.mindeseals.com and click on “ Login as a Exporter “ button. 
                                When prompted for a Username and Password please enter the following credentials :";
                                $data0="**This is an automatic generated email so please do not reply**";
                                $data2= $email_id;
                                $em= "Email ID:";
                                $or="OR";
                                $data3= $iec;
                                $i= "IEC:";
                                $data4= $rand;
                                $p= "Password:";
                                $note="<b>NOTES:</b>";
                                $data5="For detailed demo on how to purchase MIND E-Seals please check this video :<br> www.youtube.com/";
                                $data6="For detailed demo on how to link data to a purchased MIND E-Seal please see the following video :<br> www.youtube.com/";
                                $data7="For detailed demo to check if your E-Seal has been successfully read by Customs or not please see the following video :<br>
                                <YoutubeLink>";
                                $note_1="<b>1.	You must change your password after you login for the first time. To see a demo of how to change password please visit www.youtube.com/ <br>
                                2.	Please do not share your Username and Password with anybody and store it in a secure place.</b>";
                                $data8="For detailed demo on how to get Support please see this video :<br> www.youtube.com/";
                                $data9="For any other support related enquiries please write a mail to support@mindeseals.com";
                                $data10="For any other sales / marketing related enquiries please write a mail to sales@mindeseals.com";
                                //$data2= $email_id;
                                $this->email->message($data0.'<br>'.'<br>'.$data1.'<br>'.'<br>'.$em.' ' .$data2. '<br>'.$or.'<br>' .$i.' '.$data3. '<br>'.$p. ' '
                                .$data4.'<br>'.'<br>'.$note.'<br>'.$note_1.'<br>'.'<br>'.$data5.'<br>'.$data6.'<br>'.$data7.'<br>'.$data8.'<br>'.$data9.'<br>'.$data10.'<br>');
                                
                                if (! $this->email->send())
                                {
                                echo $this->email->print_debugger();
                                }
                                
                                }    
                                
                    public function view_exporter_details()
                        {
                        $data['curr_stock'] = $this->eseal_stock;
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $seg_id= $this->encrypt->decode($seg_id);
                        $this->load->model('Exporter_model');
                        $data['exporter_detail'] = $this->Exporter_model->view_exp_details($seg_id);
                        $this->load->view('Finance/view_exporter_details',$data);
                        }
                        
                    public function change_exporter_status()
                        {
                        $post = $this->input->post();
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $id= $this->encrypt->decode($seg_id);
                        $array1 = array(
                        'exporter_status' =>'Unpproved'
                        );
                        $this->load->model('Exporter_model');
                         $update =$this->Exporter_model->update_order($id,$array1);
                        //print_r($a);exit();
                         if($update==TRUE)                        {
                         $this->session->set_flashdata('msg', '<div class="alert alert-success">Status Changed Successfully</div>');
                         redirect('exporter_list'); 
                       } 
                         }

                    public function change_approve_status()
                        {
                        $post = $this->input->post();
                        $seg_id = strtr($this->uri->segment(2, 0),array('.' => '+', '-' => '=', '~' => '/'));
	                    $id= $this->encrypt->decode($seg_id);
                        $array = array(
                        'exporter_status' =>'Approved'
                        );
                        $this->load->model('Exporter_model');
                        $update =$this->Exporter_model->change_exporter_status($id,$array);
                       // print_r($update);exit();
                        if($update==TRUE)                        {
                        // $this->session->set_flashdata('msg', '<div class="alert alert-success">Status Updated Successfully</div>');
                         redirect('exporter_list');
                        
                        }
                        }
}