<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$product_id = $this->uri->segment(2);
$route['translate_uri_dashes'] = FALSE;
$route['isValid'] = "Authentication/validate";
$route['Home'] = "Authentication/HomePage";
$route['logout'] = "Authentication/logout";


//dashboard

//users

$route['add_client'] = "Staff_management/add_client";
$route['save_client'] = "Staff_management/save_client";
$route['show_client'] = "Staff_management/show_client";
$route['edit_client_details/(:any)'] = "Staff_management/edit_client_details/$product_id";
$route['update_client'] = "Staff_management/update_client";
$route['delete_client_details/(:any)'] = "Staff_management/delete_client_details/$product_id";
$route['assign_client_service'] = "Staff_management/assign_client_service";
$route['staff'] = "Staff_management/index";
$route['client_login'] = "Staff_management/client_login";
$route['isValidClient'] = "Staff_management/Staff_validate";
$route['otp'] = "Staff_management/client_otp";
$route['isValidOtp'] = "Staff_management/validate_otp";
$route['staff_dashboard'] = "Staff_management/staff_dashboard";
$route['change_pass'] = "Change_client_pass/change_pass";
$route['update_password'] = "Change_client_pass/update_password";
$route['client_logout'] = "Change_client_pass/client_logout";
$route['client_service_dashboard/(:any)'] = "Client_management/show_client_dashboard/$product_id";

//service master

$route['serviceMaster'] = "Service_management/show_service";
$route['add_service'] = "Service_management/add_service";
$route['save_service'] = "Service_management/save_service";
$route['edit_service_details/(:any)'] = "Service_management/edit_service/$product_id";
$route['update_service'] = "Service_management/update_service";
$route['delete_service_details/(:any)'] = "Service_management/delete_service_details/$product_id";

$route['save_client_service'] = "Client_management/save_client_service";
$route['add_stages'] = "Service_management/add_stages";
$route['add_checklist'] = "Service_management/add_checklist";

//Reports

$route['staff_wise_report'] = "Report/client_wise_report";
$route['company_wise_report'] = "Report/service_wise_report";
$route['get_client_wise_report'] = "Report/get_client_wise_report";
$route['get_service_wise_report'] = "Report/get_service_wise_report";

//Company  
$route['show_company'] = "Company/show_company"; 
$route['add_company'] = "Company/add_company";
$route['dashboard'] = "Company/dashboard";
$route['save_company'] = "Company/save_company"; 
$route['edit_company/(:any)'] = "Company/edit_company_details/$product_id"; 
$route['assign_to_staff'] = "Company/assign_to_staff"; 

//staff 
$route['show_staff'] = "Staff/show_staff"; 
$route['add_staff'] = "Staff/add_staff"; 

//Time sheet 
$route['show_time'] = "Timesheet/show_time"; 
$route['add_time'] = "Timesheet/add_time"; 

//Registration 
$route['show_registration'] = "Registration/show_reg"; 
$route['add_registration'] = "Registration/add_registration"; 
$route['get_searched_data'] = "Registration/get_search_registration"; 

//Signature
$route['show_signature'] = "Signature/show_sign"; 
$route['add_signature'] = "Signature/add_sign"; 
$route['get_searched_sign_data'] = "Signature/get_search_sign"; 

