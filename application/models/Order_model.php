<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Order_model extends CI_Model {
    
        public function get_count_by_exp($order_no)
                {
                    $query =$this->db->query("select  COUNT(O_ID) as total FROM order_seal
                    where ORDER_STATUS != 'send_to_dispatch' AND `ORDER_NO` LIKE '%$order_no%' ");
                    return $query->row();
                }
                
                 public function search_data($order_no,$limit,$offset)
                  {	 
                    if($limit == 0){
                    $startLimit=$limit;
                    $endLimit= 10;
                    }else{
                    $startLimit=(($limit-1) * $offset);
                    $endLimit=$offset;
                    }
                   $query =	
                $this->db->query("select  order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.O_ID,order_seal.PURCHASE_ORDER_DATE, order_seal.ORDER_QTY,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.DATE_CREATED,order_seal.ORDER_STATUS,order_seal.PAYMENT_RECEIVED_STATUS,order_seal.agent_id,order_seal.PAYMENT_PROOF,order_seal.COURIER_DETAIL,order_seal.SHIPPING_DATENTIME,exporter_login.exp_email,exporter_login.exporter_name,exporter_login.iec,exporter_login.org_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id where order_seal.ORDER_STATUS !='send_to_dispatch' AND `ORDER_NO` LIKE '%$order_no%' ORDER BY DATE_CREATED DESC LIMIT $startLimit,$endLimit ");
                 return $query->result();
                  }
    
            public function get_count()
                {
                $query =$this->db->query("select  COUNT(O_ID) as total FROM order_seal 
                where ORDER_STATUS != 'send_to_dispatch' ");
                return $query->row();
                }
          
             public function get_orders($limit,$offset)
                {	
                    if($limit == 0){
                    $startLimit=$limit;
                    $endLimit= 10;
                    }else{
                    $startLimit=(($limit-1) * $offset);
                    $endLimit=$offset;
                    }
                $query =	
                $this->db->query("select  order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.O_ID,order_seal.PURCHASE_ORDER_DATE, order_seal.ORDER_QTY,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.DATE_CREATED,order_seal.ORDER_STATUS,order_seal.PAYMENT_RECEIVED_STATUS,order_seal.agent_id,order_seal.PAYMENT_PROOF,order_seal.COURIER_DETAIL,order_seal.SHIPPING_DATENTIME,exporter_login.exp_email,exporter_login.exporter_name,exporter_login.iec,exporter_login.org_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id where order_seal.ORDER_STATUS !='send_to_dispatch' ORDER BY DATE_CREATED DESC LIMIT $startLimit,$endLimit ");
                //print_r($this->db->last_query());exit();
                return $query->result();
                }
                
                 public function get_confirm_order_by_exp($order_no)
                {
                    $query =$this->db->query("select  COUNT(O_ID) as total FROM order_seal 
                    where ORDER_STATUS = 'send_to_dispatch' AND `ORDER_NO` LIKE '%$order_no%' ");
                   // print_r($this->db->last_query());exit();
                    return $query->row();
                }
                
                public function search_confirmed_orders($order_no,$limit,$offset)
                {	
                    if($limit == 0){
                    $startLimit=$limit;
                    $endLimit= 10;
                    }else{
                    $startLimit=(($limit-1) * $offset);
                    $endLimit=$offset;
                    }
                $query =$this->db->query("select order_seal.O_ID,order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.PURCHASE_ORDER_DATE,order_seal.COURIER_DETAIL,order_seal.PAYMENT_RECEIVED_DATE, order_seal.ORDER_QTY,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.DATE_CREATED,order_seal.ORDER_STATUS,order_seal.agent_id,
                exporter_login.exp_email,exporter_login.exporter_name,exporter_login.iec,exporter_login.org_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id 
                where order_seal.ORDER_STATUS='send_to_dispatch' AND `ORDER_NO` LIKE '%$order_no%' 
                LIMIT $startLimit,$endLimit");
               // print_r($this->db->last_query());exit();
                return $query->result();
                }
                
                 public function confirmed_order_count()
                {
                    $query =$this->db->query("select  COUNT(O_ID) as total FROM order_seal 
                    where ORDER_STATUS = 'send_to_dispatch' ");
                    return $query->row();
                }
                
            public function get_confirmed_orders($limit,$offset)
                {	
                    if($limit == 0){
                    $startLimit=$limit;
                    $endLimit= 10;
                    }else{
                    $startLimit=(($limit-1) * $offset);
                    $endLimit=$offset;
                    }
                $query =	
                $this->db->query("select order_seal.O_ID,order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.PURCHASE_ORDER_DATE,order_seal.COURIER_DETAIL,order_seal.PAYMENT_RECEIVED_DATE, order_seal.ORDER_QTY,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.DATE_CREATED,order_seal.ORDER_STATUS,order_seal.agent_id,exporter_login.exp_email,exporter_login.exporter_name,exporter_login.iec,exporter_login.org_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id  where order_seal.ORDER_STATUS='send_to_dispatch' ORDER BY DATE_CREATED DESC LIMIT $startLimit,$endLimit ");
               // print_r($this->db->last_query());exit();
                return $query->result();
                }
             public function viewOrder($id)
                {	
                $query =	
                $this->db->query("select order_seal.O_ID, order_seal.ORDER_QTY,order_seal.AMOUNT,order_seal.COMMISSION,order_seal.PAYMENT_RECEIVED_DATE, order_seal.PAYMENT_RECEIVED_STATUS,order_seal.ORDER_STATUS, exporter_login.exporter_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id where order_seal.O_ID='$id'");
                return $query->result();
                }
                
            public function payment_proof($id)
                {	
                $query =	
                $this->db->query("select order_seal.ORDER_QTY,order_seal.QTY_PENDING,order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.PAYMENT_RECEIVED_STATUS,order_seal.O_ID, order_seal.PAYMENT_PROOF,order_seal.COURIER_DETAIL,order_seal.PAYMENT_RECEIVED_DATE, order_seal.ORDER_STATUS,exporter_login.exporter_name from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id where order_seal.O_ID='$id'");
                return $query->result();
                }
                
            public function courier_detail($id)
                {	
                $query =	
                $this->db->query("select O_ID,COURIER_DETAIL from order_seal where order_seal.O_ID='$id'");
                return $query->result();
                }
            public function update_order($order_array,$order_id)
                {
                $this->db->where('O_ID', $order_id);
                $this->db->update('order_seal', $order_array);
                return $this->db->affected_rows();
                }
                
            public function update_payment_status($array,$order_id)
                {
                $this->db->where('O_ID', $order_id);
                $this->db->update('order_seal', $array);
                return $this->db->affected_rows();
                }
                
            public function view_all_details($id)
                {	
                $query =	
                $this->db->query("select order_seal.IEC_ID,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.PURCHASE_ORDER_NO,order_seal.AMOUNT,order_seal.COMMISSION,order_seal.TOTAL_AMOUNT,order_seal.TOTAL_COMMISSION,order_seal.CONTACT_PERSON_NAME,
                order_seal.PHONE_NO,order_seal.SHIPPING_ADDRESS,order_seal.BILLING_ADDRESS, order_seal.ORDER_QTY,order_seal.ORDER_STATUS,order_seal.PAYMENT_PROOF,exporter_login.exporter_name,exporter_login.exp_email,exporter_login.exp_mobile from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id where order_seal.ORDER_NO='$id'");
                return $query->result();
                }
                
            public function get_pdf_data($id)
                {	
                $query =	
                $this->db->query("select order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.ORDER_NO,order_seal.ORDER_THROUGH,order_seal.AMOUNT,order_seal.TOTAL_AMOUNT,order_seal.ORDER_QTY,order_seal.BILLING_GSTN,order_seal.CONTACT_PERSON_NAME,order_seal.BILLING_ADDRESS,order_seal.BILLING_ADD_PINCODE,
                order_seal.PHONE_NO,agent_login.agent_email,exporter_login.exporter_name,exporter_login.state_name,exporter_login.exp_email,exporter_login.exp_mobile,exporter_login.iec,exporter_login.gst_no,exporter_login.bill_address,exporter_login.shipped_address,exporter_login.pincode,States.state_code from order_seal 
                left join exporter_login ON order_seal.Exp_id=exporter_login.Exp_id 
                left join agent_login ON order_seal.agent_id=agent_login.agent_id
                left join States ON exporter_login.state_name=States.StateName
                where order_seal.O_ID='$id'");
                return $query->result();
                }
                
            public function get_agentpdf_data($id)
                {	
                $query =	
                $this->db->query("select order_seal.QTY_TO_ALLOCATE_THIS_TIME,order_seal.ORDER_THROUGH,order_seal.AMOUNT,order_seal.TOTAL_AMOUNT,order_seal.ORDER_QTY,
                agent_login.agent_email,agent_login.agent_mobile,agent_login.gst_no,agent_login.billing_address,agent_login.shipping_address,agent_login.Iec_no,agent_login.pin_code,agent_login.agent_name,agent_login.agent_logo from order_seal left join agent_login ON order_seal.agent_id=agent_login.agent_id where order_seal.O_ID='$id'");
                return $query->result();
                }
                
             public function insert_payment_status($array1)
                {
                $this->db->insert('orders_installment',$array1);
                $id = $this->db->insert_id();
                return $id;
                }
            public function get_order_qty($order_id)
                {	
                $query =	
                $this->db->query("select order_seal.O_ID,order_seal.ORDER_QTY from order_seal where O_ID='$order_id' ");
                return $query->result();
                }
                
                public function get_qty($order_id)
                {	
                $query =	
                $this->db->query("select SUM(QUANTITY) as sum from orders_installment where O_ID='$order_id' ");
                return $query->result();
                }
                
            public function view_link_details($seg_id)
                {	
                $query =	
                $this->db->query("select order_seal.ORDER_NO,order_seal.O_ID,orders_installment.QUANTITY,orders_installment.DATE from order_seal left join orders_installment on order_seal.O_ID=orders_installment.O_ID where ORDER_NO = '$seg_id' ");
                //print_r($this->db->last_query());exit();
                return $query->result();
                }
}
