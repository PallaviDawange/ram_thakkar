<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Company_manage_model extends CI_Model {

	           public function save_company_data($array)
                {
                $this->db->insert('company_master',$array);
                $id = $this->db->insert_id();
                return $id;
                }

               public function show_company_data()
                {	
                $query =	
                $this->db->query("select * from company_master ");
                return $query->result();
                }

               public function edit_company_data($comp_id)
                {	
                $query =	
                $this->db->query("select * from company_master where comp_id ='$comp_id'");
                return $query->result();
                }
               public function update_client($array,$id)
                {
                $this->db->where('client_id', $id);
                $this->db->update('client_master', $array);
                return $this->db->affected_rows();
                }

              public function delete_client($id)
                {
                $this->db->where('client_id', $id);
                $this->db->delete('client_master');
                return $this->db->affected_rows();
                }

            public function check_client_mobile($mobile)
                { 
                $query =  
                $this->db->query("select * from client_master  where mobile='$mobile'");    
                return $query->row();
                }
          public function update_otp($array,$id)
                {
                $this->db->where('client_id', $id);
                $this->db->update('client_master', $array);
                return $this->db->affected_rows();
                }

                 public function check_client_otp($otp)
                { 
                $query =  
                $this->db->query("select client_id,otp from client_master  where otp='$otp'");    
                return $query->row();
                }
                

                public function show_logged_client_data($id)
                { 
                $query =  
                $this->db->query("select * from client_master left join service_master on client_master.sv_id=service_master.sv_id
                 left join client_services on client_master.client_id=client_services.client_id
                 where client_master.client_id='$id'");    
                return $query->result();
                }

                 public function show_client_services($id)
                { 
                $query =  
                $this->db->query("select service_master.service_name, service_master.frequency,client_services.completed_date,client_services.status,client_services.client_id from client_services left join service_master on client_services.sv_id=service_master.sv_id 
left join client_master on client_services.client_id=client_master.client_id
where client_services.client_id='$id' ORDER BY completed_date DESC");  
                 //print_r($this->db->last_query());exit();  
                return $query->result();
                }


            public function update_pass($id,$array)
                {
                $this->db->where('client_id', $id);
                $this->db->update('client_master', $array);
                return $this->db->affected_rows();
                }

                 public function check_client_password($mobile,$password)
                { 
                    //print_r($pass);exit();
                $query = $this->db->query("select client_id,password from client_master where mobile='$mobile'and password='$password'");  
               // print_r($this->db->last_query()) ;exit(); 
                return $query->row();
                }


               public function get_services()
                {   
                $query =    
                $this->db->select('*') 
                         ->from('service_master')
                         ->get();
                return $query->result();
                }

                public function get_selected($client_id)
                {
                    $query =    
                $this->db->select('*') 
                         ->from('service_master')
                         ->join('client_services','client_services.sv_id=service_master.sv_id','left')
                         ->where('client_services.client_id',$client_id)
                         ->get();
                return $query->result();
                }

                public function store_services($array)
                {
                    $this->db->insert('client_services',$array);
                    return $this->db->insert_id();
                }

                public function deleted_previous($client_id)
                {
                    $this->db->where('client_id',$client_id)
                             ->delete('client_services');
                    return $this->db->affected_rows();
                }

                public function get_one_client($client_id)
                {
                    $query = $this->db->select('*')
                                      ->from('client_master')
                                      ->where('client_id',$client_id)
                                      ->get();
                    return $query->result();
                }

                public function getClientServices($client_id )
                {
                    $query = $this->db->select('*')
                                      ->from('client_services')
                                      ->join('service_master','service_master.sv_id=client_services.sv_id','left')
                                      ->where('client_services.client_id',$client_id)
                                      ->order_by('service_master.sv_id','desc')
                                      ->get();
                    return $query->result();
                }

                public function update_status($array,$id)
                {
                    $this->db->where('cs_id',$id)
                            ->update('client_services',$array);
                    return $this->db->affected_rows();
                }
                
}