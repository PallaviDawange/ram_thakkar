<?php 

class Service_manage_model extends CI_Model{

	public function get_services()
	{
		$query = $this->db->select('*')
		                  ->from('service_master')
		                  ->get();
		return $query->result();
	}

	public function store_service($array)
	{
		$this->db->insert('service_master',$array);
		return $this->db->insert_id();
	}

	public function get_one_service($service_id)
	{
		$query = $this->db->select('*')
		                  ->from('service_master')
		                  ->where('sv_id',$service_id)
		                  ->get();
		return $query->result();
	}

	public function update_service($sv_id,$array)
	{
		$this->db->where('sv_id',$sv_id)
		         ->update('service_master',$array);
		return $this->db->affected_rows();        
	}

	public function delete_service_details($service_id)
	{
		$this->db->where('sv_id',$service_id)
		         ->delete('service_master');
		return $this->db->affected_rows();
	}
}


?>